<!-- BEGIN HEADER -->
<!-- 顶部 -->
<div class="layui-header">
    <!-- 头部区域 -->
    <ul class="layui-nav layui-layout-left">
        <li class="layui-nav-item layadmin-flexible" lay-unselect>
            <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
                <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
            </a>
        </li>
        <li class="layui-nav-item">
            <a href="javascript:;" data-url="{{url('admin/delcache')}}" class="refresh-btn" title="刷新">
                <i class="layui-icon layui-icon-refresh-3"></i>
            </a>
        </li>
        <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <input type="text" class="layui-input layui-input-search" placeholder="搜索..." autocomplete="off">
        </li>
    </ul>
    <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
        <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="theme">
                <i class="layui-icon layui-icon-theme"></i>
            </a>
        </li>
        <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="fullscreen">
                <i class="layui-icon layui-icon-screen-full"></i>
            </a>
        </li>
        <li class="layui-nav-item" lay-unselect style="margin-right: 30px;">
            <a href="javascript:;">
                <img class="layui-nav-img userAvatar" src="{{asset('static/admin/img/avatar.png')}}" width="35" height="35">
                <cite>{{$adminInfo['name']}}</cite>
            </a>
            <dl class="layui-nav-child">
                <dd style="text-align: center;"><a href="javascript:;" id="view-profile" data-url="{{url('admin/profile')}}">基本资料</a></dd>
                <dd style="text-align: center;"><a href="javascript:;" id="view-reset_password" data-url="{{url('admin/resetPassword')}}">修改密码</a></dd>
                <hr>
                <dd style="text-align: center;"><a href="{{route('admin.logout')}}">退出</a></dd>
            </dl>
        </li>
    </ul>
</div>
<!-- END HEADER -->
