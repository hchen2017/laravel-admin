<!-- BEGIN FOOTER -->
<div class="layui-footer footer">
    <div class="page-footer-inner">
        2020-2028 &copy; <a href="https://github.com">Github</a>.</strong> All rights reserved.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->