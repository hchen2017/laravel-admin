<!-- BEGIN SIDEBAR -->
<div class="layui-side layui-side-menu">
    <div class="layui-side-scroll">
        <div class="layui-logo">
            <span style="font-size: 18px;font-weight: bold;">后台管理系统</span>
        </div>

        <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
            {!! $sidebar !!}
        </ul>
    </div>
</div>
