@extends('admin.layouts.iframe')

@section('content')
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <form class="layui-form">
                <div class="layui-form-item searchBox">
                    <div class="layui-inline">
                        <!--<button type="button" class="layui-btn layui-btn-normal check-all">
                            <i class="layui-icon icon-check layuiadmin-button-btn"></i> 全选
                        </button>-->
                        <button type="reset" class="layui-btn layui-btn-warm export-btn">
                            <i class="layui-icon icon-control-start layuiadmin-button-btn"></i> 备份
                        </button>
                        <button type="button" class="layui-btn optimize-btn" data-url="{{url(strtolower($model).'/optimize')}}">优化表</button>
                        <button type="button" class="layui-btn repair-btn" data-url="{{url(strtolower($model).'/repair')}}">修复表</button>
                    </div>

                    <div class="layui-inline">

                    </div>

                    <div class="layui-inline" style="float: right;">

                    </div>
                </div>
            </form>
        </div>

        <div class="layui-card-body">
            <form id="export-form" class="js-ajax-form" action="" method="post">
                <table id="exportTable" lay-filter="exportTable"></table>

                <!--<script type="text/html" id="exportToolbar">
                    <div class="layui-btn-container">
                        <button class="layui-btn layui-btn-sm" lay-event="getCheckData">获取选中行数据</button>
                        <button class="layui-btn layui-btn-sm" lay-event="getCheckLength">获取选中数目</button>
                        <button class="layui-btn layui-btn-sm" lay-event="isAll">验证是否全选</button>
                    </div>
                </script>-->
            </form>
        </div>
    </div>
</div>

{!! csrf_field() !!}
<input type="hidden" class="exportlist_url" value="{{url(strtolower($model).'/export')}}">
<input type="hidden" class="exportpost_url" value="{{url(strtolower($model).'/exportPost')}}">
<input type="hidden" class="optimize_url" value="{{url(strtolower($model).'/optimize')}}">
<input type="hidden" class="repair_url" value="{{url(strtolower($model).'/repair')}}">
@endsection

@section('page_script')
    <script src="{{asset('static/admin/pages/js/database.js')}}" type="text/javascript"></script>
@endsection
