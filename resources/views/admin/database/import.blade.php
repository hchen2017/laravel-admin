@extends('admin.layouts.iframe')

@section('content')
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <table id="importTable" lay-filter="importTable"></table>
        </div>
    </div>
</div>

{!! csrf_field() !!}
<input type="hidden" class="importlist_url" value="{{url(strtolower($model).'/import')}}">
<input type="hidden" class="importpost_url" value="{{url(strtolower($model).'/importPost')}}">
<input type="hidden" class="download_url" value="{{url(strtolower($model).'/download')}}">
<input type="hidden" class="delete_url" value="{{url(strtolower($model).'/delete')}}">
@endsection

@section('page_script')
    <script src="{{asset('static/admin/pages/js/database.js')}}" type="text/javascript"></script>
@endsection
