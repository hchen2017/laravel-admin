@extends('admin.layouts.iframe')

@section('content')
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body" style="padding: 15px;">
                <form class="layui-form" action="">
                    {!! csrf_field() !!}
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>手机号</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="phone" value="{{$adminInfo['phone']}}" lay-verify="required" placeholder="" autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>原始密码</label>
                        <div class="layui-input-block">
                            <input type="password" class="layui-input" id="password" name="password" lay-verify="required" placeholder="" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>新密码</label>
                        <div class="layui-input-block">
                            <input type="password" class="layui-input" id="newpassword" name="newpassword" lay-verify="required" placeholder="" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>确认密码</label>
                        <div class="layui-input-block">
                            <input type="password" class="layui-input" id="confirmpassword" name="confirmpassword" lay-verify="required" placeholder="" autocomplete="off">
                        </div>
                    </div>

                    <div class="layui-form-item layui-layout-admin">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button type="button" class="layui-btn layui-btn-primary close-btn">取消</button>
                                <button type="button" class="layui-btn laySave" lay-submit="" lay-filter="laySave">保存 <i class="layui-icon layui-icon-release"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" class="save_url" value="{{url('admin/resetPassword')}}">
    <input type="hidden" class="logout_url" value="{{url('admin/logout')}}">
@endsection

@section('page_script')
    <script type="text/javascript">
        layui.use(['form', 'layer'], function () {
            var form = layui.form,
                layer = parent.layer === undefined ? layui.layer : top.layer,
                $ = layui.jquery;

            var save_url = $('.save_url').val();


            // 表单提交
            form.on("submit(laySave)", function (data) {
                var password = $('#password').val();
                var newpassword = $('#newpassword').val();
                var confirmpassword = $('#confirmpassword').val();

                if (password == newpassword) {
                    layer.msg('新密码不能和原始密码一样！');
                    $('#newpassword').focus();
                    return false;
                }
                if (newpassword != confirmpassword) {
                    layer.msg('两次密码不一致！');
                    $('#confirmpassword').focus();
                    return false;
                }

                submitForm(data);
            });

            // 表单提交
            function submitForm(data) {
                // 弹出loading
                var index = layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});

                $.ajax({
                    url: save_url,
                    data: data.field,
                    type: "post",
                    dataType: "json",
                    success: function (res) {
                        top.layer.close(index);
                        top.layer.msg(res.msg);
                        if (res.code == 1) {
                            layer.closeAll("iframe");
                            // 退出登录
                            var logout_url = $('.logout_url').val();
                            $.get(logout_url, function (res) {
                                layer.msg('退出登录');
                                parent.window.location.reload();
                            });
                        }
                    },
                    error: function (data) {
                        layer.msg("服务器无响应");
                    }
                });
            }
        });
    </script>
@endsection
