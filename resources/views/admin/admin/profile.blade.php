@extends('admin.layouts.iframe')

@section('content')
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body" style="padding: 15px;">
                <form class="layui-form" action="">
                    {!! csrf_field() !!}
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>用户名</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="name" value=" {{$adminInfo['name']}}"  maxlength="12" lay-verify="required" placeholder="" autocomplete="off" >
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>手机号</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input phone" name="phone" value="{{$adminInfo['phone']}}" data-phone="{{$adminInfo['phone']}}" maxlength="11" lay-verify="required" placeholder="" autocomplete="off" >
                        </div>
                    </div>

                    <div class="layui-form-item layui-layout-admin">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button type="button" class="layui-btn layui-btn-primary close-btn">取消</button>
                                <button type="button" class="layui-btn" lay-submit="" lay-filter="laySave">保存 <i class="layui-icon layui-icon-release"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" class="save_url" value="{{url('admin/profile')}}">
    <input type="hidden" class="check_phone_url" value="{{url(strtolower($model).'/checkPhone')}}">
@endsection

@section('page_script')
    <script type="text/javascript">
        layui.use(['form', 'layer'], function () {
            var form = layui.form,
                layer = parent.layer === undefined ? layui.layer : top.layer,
                $ = layui.jquery;

            var check_phone_url = $('.check_phone_url').val();
            var save_url = $('.save_url').val();


            // 表单提交
            form.on("submit(laySave)", function (data) {
                var phone = $('.phone').val();
                var ori_phone = $('.phone').attr('data-phone');
                if (phone != ori_phone) {
                    var url = check_phone_url;
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            '_token': $('input[name=_token]').val(),
                            'phone': phone
                        },
                        dataType: 'json',
                        success: function (res) {
                            if (res.code == 1) {
                                layer.msg('该手机号已经存在！', {icon: 5});
                                $('.phone').focus();
                                return false;
                            } else {
                                submitForm(data);
                            }
                        }
                    });
                    return false;
                }

                submitForm(data);
            });

            // 表单提交
            function submitForm(data) {
                // 弹出loading
                var index = layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});

                $.ajax({
                    url: save_url,
                    data: data.field,
                    type: "post",
                    dataType: "json",
                    success: function (res) {
                        top.layer.close(index);
                        top.layer.msg(res.msg);
                        if (res.code == 1) {
                            layer.closeAll("iframe");
                            // 刷新父页面
                            parent.location.reload();
                        }
                    },
                    error: function (data) {
                        layer.msg("服务器无响应");
                    }
                });
            }
        });
    </script>
@endsection
