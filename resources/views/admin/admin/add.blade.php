@extends('admin.layouts.iframe')

@section('content')
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body" style="padding: 15px;">
                <form class="layui-form" action="">
                    {!! csrf_field() !!}
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>名称</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input name" name="name" value="" data-name="" maxlength="40" lay-verify="required" placeholder="请输入" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>手机号</label>
                        <div class="layui-input-inline">
                            <input type="number" class="layui-input phone" name="phone" value="" data-phone="" maxlength="11" lay-verify="phone" placeholder="请输入" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>登录密码</label>
                        <div class="layui-input-inline">
                            <input type="password" class="layui-input password" name="password" value="" lay-verify="password" placeholder="请输入密码" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>确认密码</label>
                        <div class="layui-input-inline">
                            <input type="password" class="layui-input confirm_password" name="confirm_password" value="" lay-verify="confirm_password" placeholder="请输入密码" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>角色</label>
                        <div class="layui-input-block">
                            @foreach($roles as $role)
                                <input type="checkbox" class="role_ids" name="role_ids[]" value="{{$role['id']}}" lay-skin="primary" title="{{$role['name']}}">
                            @endforeach
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">是否启用</label>
                        <div class="layui-input-inline">
                            <input type="checkbox" name="status" checked="" lay-skin="switch" lay-text="启用|禁用">
                        </div>
                    </div>

                    <input type="hidden" name="id" value="">

                    <div class="layui-form-item layui-layout-admin layui-hide">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button type="button" class="layui-btn layui-btn-primary close-btn">取消</button>
                                <button type="button" class="layui-btn laySave" lay-submit="" lay-filter="laySave">保存 <i class="layui-icon layui-icon-release"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" class="save_url" value="{{url(strtolower($model).'/add')}}">
@endsection

@section('page_script')
    <script src="{{asset('static/admin/pages/js/admin.js')}}" type="text/javascript"></script>
@endsection
