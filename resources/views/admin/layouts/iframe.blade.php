<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统 - {{$title ?? 'Admin'}}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <base href="{{url('/')}}"/>

    <link href="{{asset('static/plugins/layui/css/layui.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('static/admin/css/admin.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('static/admin/css/custom.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('static/plugins/simple-line-icons/layui-icon.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('static/plugins/viewer/css/viewer.css')}}" rel="stylesheet" type="text/css"/>

    @yield('page_css')

    <script type="text/javascript">
        // 全局变量
        // 全局变量
        var APP = {
            HOST: "{{$_SERVER['HTTP_HOST']}}",
            //ROOT: "{{asset('/')}}",
            ROOT: "{{url('/')}}/",
            STATIC : "{{asset('static')}}/",
        };
    </script>
</head>

<body class="">
<div>
    @yield('content')

    <input type="hidden" class="msg_success" @if(session('success')) value="{{session('success')}}" @endif />
    <input type="hidden" class="msg_error" @if(session('error')) value="{{session('error')}}" @endif />
    <input type="hidden" class="upload_url" value="{{route('admin.upload')}}">
</div>

<script src="{{asset('static/plugins/layui/layui.js')}}" type="text/javascript"></script>
<script src="{{asset('static/admin/js/common.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    layui.config({
        base: "{{asset('static/admin')}}/" // 静态资源所在路径
    }).extend({
        app: 'js/app' // 主入口模块
    }).use('app');
</script>

@yield('page_script')
</body>
</html>
