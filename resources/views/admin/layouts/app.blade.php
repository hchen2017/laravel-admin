<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统 - {{$title ?? 'Admin'}}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <base href="{{url('/')}}"/>

    <script src="{{asset('static/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
    <link href="{{asset('static/admin/css/backend.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('static/plugins/simple-line-icons/layui-icon.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('static/plugins/viewer/css/viewer.css')}}" rel="stylesheet" type="text/css"/>

    <link rel="icon" href="{{asset('favicon.ico')}}">

    @yield('page_css')

    <script type="text/javascript">
        // 全局变量
        var APP = {
            HOST: "{{$_SERVER['HTTP_HOST']}}",
            //ROOT: "{{asset('/')}}",
            ROOT: "{{url('/')}}/",
            STATIC : "{{asset('static')}}/",
        };
    </script>
</head>

<body class="layui-layout-body">
<div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
        <!-- 顶部 -->
        @include('admin.partials.header')

        <!-- 左侧导航 -->
        @include('admin.partials.sidebar')

        <!-- 页面标签 -->
        <div class="layadmin-pagetabs" id="LAY_app_tabs">
            <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-down">
                <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;"></a>
                        <dl class="layui-nav-child layui-anim-fadein">
                            <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                            <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                            <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
                <ul class="layui-tab-title" id="LAY_app_tabsheader">
                    <li lay-id="{{url('admin/dashboard')}}" lay-attr="{{url('admin/dashboard')}}" class="layui-this"><i class="layui-icon layui-icon-home"></i></li>
                </ul>
            </div>
        </div>

        <!-- 主体内容 -->
        <div class="layui-body" id="LAY_app_body">
            <div class="layadmin-tabsbody-item layui-show">
                <iframe src="{{url('admin/dashboard')}}" frameborder="0" class="layadmin-iframe"></iframe>
            </div>
        </div>

        <!-- 辅助元素，一般用于移动设备下遮罩 -->
        <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
</div>

<input type="hidden" class="msg_success" @if(session('success')) value="{{session('success')}}" @endif />
<input type="hidden" class="msg_error" @if(session('error')) value="{{session('error')}}" @endif />
<input type="hidden" class="upload_url" value="{{route('admin.upload')}}">

<script src="{{asset('static/plugins/layui/layui.js')}}" type="text/javascript"></script>
<script src="{{asset('static/admin/js/common.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    layui.config({
        base: "{{asset('static/admin')}}/" // 静态资源所在路径
    }).extend({
        app: 'js/app' // 主入口模块
    }).use('app');
</script>

@yield('page_script')
</body>
</html>
