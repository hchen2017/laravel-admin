@extends('admin.layouts.iframe')

@section('content')
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-form layui-card-header layuiadmin-card-header-auto">
                <form class="layui-form">
                    <div class="layui-form-item searchBox">
                        <div class="layui-inline">
                            <label class="layui-form-label">创建时间</label>
                            <div class="layui-input-block" style="width: 300px;">
                                <input type="text" class="layui-input lay-date-range" name="range_date" value="" placeholder="创建时间" autocomplete="off">
                            </div>
                        </div>
                        <input type="text" class="layui-input" value="阻止回车键提交" style="display: none;">

                        <div class="layui-inline">
                            <button type="button" class="layui-btn search_btn">
                                <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                            </button>
                            <button type="reset" class="layui-btn layui-btn-danger reset_btn"><i class="layui-icon layui-icon-refresh layuiadmin-button-btn"></i></button>
                        </div>

                        <div class="layui-inline" style="float: right;">

                        </div>
                    </div>
                </form>
            </div>

            <div class="layui-card-body">
                <table id="dataTable" lay-filter="dataTable"></table>
            </div>
        </div>
    </div>

    {!! csrf_field() !!}
    <input type="hidden" class="datalist_url" value="{{url(strtolower($model))}}">
    <input type="hidden" class="delete_url" value="{{url(strtolower($model).'/delete')}}">
@endsection

@section('page_script')
    <script src="{{asset('static/admin/pages/js/adminlog.js')}}" type="text/javascript"></script>
@endsection
