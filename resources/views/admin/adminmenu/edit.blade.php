@extends('admin.layouts.iframe')

@section('content')
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body" style="padding: 15px;">
                <form class="layui-form" action="">
                    {!! csrf_field() !!}
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>名称</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input name" name="name" value="{{$info['name']}}" data-name="" maxlength="40" lay-verify="required" placeholder="请输入" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>上级菜单</label>
                        <div class="layui-input-inline">
                            <select class="parent_id" name="parent_id" lay-verify="required">
                                @foreach($menuOptions as $menukey => $menu)
                                    <option value="{{$menukey}}" @if($info['parent_id'] == $menukey) selected @endif >{!! $menu !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">应用名</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="app" value="{{$info['app']}}" placeholder="请输入" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">控制器</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="controller" value="{{$info['controller']}}" placeholder="请输入" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">方法</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="action" value="{{$info['action']}}" placeholder="请输入" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">路由</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="route" value="{{$info['route']}}" placeholder="请输入" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>类型</label>
                        <div class="layui-input-block">
                            <input type="radio" name="type" value="1" title="模块" @if($info['type'] == 1) checked="" @endif >
                            <input type="radio" name="type" value="2" title="列表" @if($info['type'] == 2) checked="" @endif >
                            <input type="radio" name="type" value="3" title="操作" @if($info['type'] == 3) checked="" @endif >
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">图标</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input icon_name" name="icon" value="{{$info['icon']}}" maxlength="30" placeholder="" autocomplete="off">
                        </div>
                        <span class="layui-btn layui-btn-primary icon_design" style="padding:0 12px;min-width:45px"><i style="font-size:1.2em;margin:0" class="layui-icon {{$info['icon']}}"></i></span>
                        <button type="button" class="layui-btn layui-btn-primary select-icon">选择</button>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>排序</label>
                        <div class="layui-input-inline">
                            <input type="number" class="layui-input" name="sorting" value="{{$info['sorting']}}" maxlength="10" lay-verify="required" placeholder="100" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">是否启用</label>
                        <div class="layui-input-inline">
                            <input type="checkbox" name="status" @if($info['status'] == 1) checked="" @endif lay-skin="switch" lay-text="启用|禁用">
                        </div>
                    </div>

                    <input type="hidden" name="id" value="{{$info['id']}}">

                    <div class="layui-form-item layui-layout-admin">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button type="button" class="layui-btn layui-btn-primary close-btn">取消</button>
                                <button type="button" class="layui-btn laySave" lay-submit="" lay-filter="laySave">保存 <i class="layui-icon layui-icon-release"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" class="save_url" value="{{url(strtolower($model).'/edit/'.$info['id'])}}">
    <input type="hidden" class="icons_url" value="{{url('admin/iframe/icons')}}">
@endsection

@section('page_script')
    <script src="{{asset('static/admin/pages/js/adminmenu.js')}}" type="text/javascript"></script>
@endsection
