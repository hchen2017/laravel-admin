@extends('admin.layouts.iframe')

@section('content')
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-form layui-card-header layuiadmin-card-header-auto">
                <form class="layui-form">
                    <div class="layui-form-item searchBox">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-warm open-btn">
                                <i class="layui-icon layui-icon-down layuiadmin-button-btn"></i>展开
                            </button>
                        </div>

                        <div class="layui-inline">

                        </div>

                        <div class="layui-inline" style="float: right;">
                            <a href="javascript:;" class="layui-btn layui-btn-normal add_btn"><i class="layui-icon layui-icon-add-circle-fine layuiadmin-button-btn"></i> 新增菜单</a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="layui-card-body">
                <form class="layui-form">
                    <table class="layui-table" id="list-table">
                        <thead>
                        <tr>
                            <th lay-data="{minWidth:100}">排序</th>
                            <th lay-data="{minWidth:160}">名称</th>
                            <th lay-data="">应用</th>
                            <th lay-data="">控制器</th>
                            <th lay-data="">方法</th>
                            <th lay-data="">路由</th>
                            <th lay-data="">状态</th>
                            <th lay-data="{minWidth:140}">操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($datas as $data)
                            <tr ole="row" class="{{$data['level']}}" id="{{$data['level']}}_{{$data['id']}}" @if($data['level'] > 1) style="display: none;" @endif >
                                <td align="left" style="width: 140px;padding-left: {{$data['level']*2}}em">
                                    @if(isset($data['has_children']) && $data['has_children'] == 1)
                                    <span class="layui-icon layui-icon-triangle-r layui-btn layui-btn-xs open-child-btn" id="icon_{{$data['level']}}_{{$data['id']}}"></span>&nbsp;
                                    @endif
                                    <span>
                                    <input type="hidden" class="layui-input sorting_ids" name="sorting_ids[]" value="{{$data['id']}}" >
                                    <input type="text" class="layui-input sorting" name="sorting" value="{{$data['sorting']}}" size="5" style="display: inline-block;width: 100px;" >
                                </span>
                                </td>
                                <td align="left">{!!$data['name']!!}</td>
                                <td>{{$data['app']}}</td>
                                <td>{{$data['controller']}}</td>
                                <td>{{$data['action']}}</td>
                                <td>{{$data['route']}}</td>
                                <td>
                                    @if($data['status'] == 1)
                                        <button type="button" class="layui-btn layui-btn-normal layui-btn-xs">显示</button>
                                    @elseif($data['status'] == 2)
                                        <button type="button" class="layui-btn layui-btn-primary layui-btn-xs">隐藏</button>
                                    @else
                                        <button type="button" class="layui-btn layui-btn-danger layui-btn-xs">删除</button>
                                    @endif
                                </td>
                                <td>
                                    <button type="button" class="layui-btn layui-btn-primary layui-btn-xs addChild_btn" data-id="{{$data['id']}}">添加子菜单</button>
                                    <button type="button" class="layui-btn layui-btn-normal layui-btn-xs edit_btn" data-id="{{$data['id']}}"><i class="layui-icon layui-icon-edit"></i>编辑</button>
                                    <button type="button" class="layui-btn layui-btn-danger layui-btn-xs del_btn" data-id="{{$data['id']}}"><i class="layui-icon layui-icon-delete"></i>删除</button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>

    {!! csrf_field() !!}
    <input type="hidden" class="datalist_url" value="{{url(strtolower($model))}}">
    <input type="hidden" class="add_url" value="{{url(strtolower($model).'/add')}}">
    <input type="hidden" class="edit_url" value="{{url(strtolower($model).'/edit')}}">
    <input type="hidden" class="delete_url" value="{{url(strtolower($model).'/delete')}}">
    <input type="hidden" class="sorting_url" value="{{url(strtolower($model).'/listOrders')}}">
@endsection

@section('page_script')
    <script src="{{asset('static/admin/pages/js/adminmenu.js')}}" type="text/javascript"></script>
@endsection
