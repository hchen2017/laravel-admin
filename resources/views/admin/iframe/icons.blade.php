@extends('admin.layouts.iframe')

@section('page_css')
    <style>
        .doc-icon {
            font-size: 0;
        }
        .doc-icon li {
            display: inline-block;
            vertical-align: middle;
            width: 20%;
            /*height: 65px;*/
            line-height: 25px;
            padding: 20px 0;
            margin-right: -1px;
            margin-bottom: -1px;
            border: 1px solid #e2e2e2;
            font-size: 14px;
            text-align: center;
            color: #666;
            cursor: pointer;
            transition: all .3s;
            -webkit-transition: all .3s;
        }
        .doc-icon li:hover {
            color: #fff;
            background-color: #5FB878;
        }
        .doc-icon li .layui-icon {
            display: inline-block;
            font-size: 36px;
        }
        .doc-icon li .doc-icon-fontclass {
            height: 30px;
            line-height: 20px;
            padding: 0 5px;
            font-size: 13px;
            color: #333;
        }
    </style>
@endsection

@section('content')
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-tab layui-tab-brief" lay-filter="">
                    <ul class="layui-tab-title">
                        <li class="layui-this"> Layui 图标 <span class="layui-badge">{{count($datas['layuis'])}}</span></li>
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <ul class="doc-icon layui-row">
                                @foreach($datas['layuis'] as $layui)
                                <li class="layui-col-sm4 layui-col-md3 layui-col-lg2">
                                    <i class="layui-icon {{$layui}}" data-class="{{$layui}}"></i>
                                    <div class="doc-icon-fontclass">{{$layui}}</div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="layui-tab-item">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_script')

@endsection
