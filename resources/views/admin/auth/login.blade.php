<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理 - 登录</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link href="{{asset('static/plugins/layui/css/layui.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('static/admin/css/admin.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('static/admin/css/login.css')}}" rel="stylesheet" type="text/css"/>

    <link rel="icon" href="{{asset('favicon.ico')}}">
</head>
<body id="login-body" class="layui-layout-body">
<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">
    <div class="layadmin-user-login-main" id="login-main">
        <form class="layui-form login-form" action="{{route('admin.doLogin')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" >
            {{--{!! csrf_field() !!}--}}
            <!--<div class="layadmin-user-login-box layadmin-user-login-header">
                <h2>后台登录</h2>
            </div>-->
            <div class="login_face"><img src="{{asset('static/admin/img/avatar.png')}}" class="userAvatar"></div>
            <div class="layadmin-user-login-box layadmin-user-login-body">
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="phone"></label>
                    <input type="text" class="layui-input" id="name" name="name" lay-verify="required" placeholder="用户名">
                </div>
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="password"></label>
                    <input type="password" class="layui-input" id="password" name="password" lay-verify="required" placeholder="密码">
                </div>
                <div class="layui-form-item" style="margin-bottom: 20px;">
                    <input type="checkbox" name="remember" lay-skin="primary" title="记住密码">
                    {{--<a href="javascript:;" class="layadmin-user-jump-change layadmin-link" style="margin-top: 7px;">忘记密码？</a>--}}
                </div>
                <div class="layui-form-item">
                    <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="login">登 录</button>
                </div>
            </div>
        </form>
    </div>

    <div class="layui-trans layadmin-user-login-footer">

    </div>
</div>

<script type="text/javascript" src="{{asset('static/plugins/layui/layui.js')}}"></script>
<script type="text/javascript">
    layui.use(['form', 'layer', 'jquery'], function () {
        var form = layui.form,
            layer = parent.layer === undefined ? layui.layer : top.layer;
        var $ = layui.jquery;


        // 登录按钮
        form.on('submit(login)', function (data) {
            var _this = $(this);
            $(this).text('登录中...').attr('disabled', true).addClass('layui-disabled');

            var login_url = _this.parents('form').attr('action');
            $.ajax({
                url: login_url,
                type: 'post',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data.field),
            }).done(function (res) {
                layer.msg(res.msg);
                _this.text('登 录').attr('disabled', false).removeClass('layui-disabled');
                if (res.code == 1) {
                    //window.location.href = res.url;
                    //window.parent.location.href = res.url;
                    parent.location.reload(); // 父页面刷新
                }
            });
            return false;
        });

        // 表单输入效果
        $('.loginBody .input-item').on('click', function (e) {
            e.stopPropagation();
            $(this).addClass('layui-input-focus').find('.layui-input').focus();
        });
        $('.loginBody .layui-form-item .layui-input').on('focus', function () {
            $(this).parent().addClass('layui-input-focus');
        });
        $('.loginBody .layui-form-item .layui-input').on('blur', function () {
            $(this).parent().removeClass('layui-input-focus');
            if ($(this).val() != '') {
                $(this).parent().addClass('layui-input-active');
            } else {
                $(this).parent().removeClass('layui-input-active');
            }
        });
    });
</script>
</body>
</html>
