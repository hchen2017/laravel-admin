@extends('admin.layouts.iframe')

@section('content')
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body" style="padding: 15px;">
                <form class="layui-form" action="">
                    {!! csrf_field() !!}
                    <div class="layui-tab layui-tab-brief" lay-filter="component-tabs-brief">
                        <ul class="layui-tab-title">
                            <li class="layui-this">基本信息</li>
                            <li>详情</li>
                        </ul>
                        <div class="layui-tab-content">
                            <div class="layui-tab-item layui-show">
                                <div class="layui-form-item">
                                    <label class="layui-form-label"><span class="required">*</span>名称</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input name" name="name" value="{{$info['name']}}" data-name="" maxlength="40" lay-verify="required" placeholder="请输入" autocomplete="off">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label"><span class="required">*</span>分类</label>
                                    <div class="layui-input-inline">
                                        <select class="cate_id" name="cate_id" lay-verify="required">
                                            <option value="">请选择</option>
                                            @foreach($articleCates as $articleCate)
                                                <option value="{{$articleCate['id']}}" @if($info['cate_id'] == $articleCate['id'])} selected="" @endif>{{$articleCate['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label"><span class="required">*</span>封面图</label>
                                    <div class="layui-input-block">
                                        <div id="upload-file" class="upload_file-box">
                                            <input type="hidden" class="layui-input cover_img" name="cover_img" value="{{$info['cover_img']}}" lay-verify="cover_img">
                                            <span id="upload-btn" class="upload-btn" data-multi="false" data-field="cover_img" data-total_count="1" data-count="0">
                                                <img class="load_img" src="{{$info['full_cover_img'] ? $info['full_cover_img'] : asset('static/admin/img/default.png')}}" alt="图片上传" style="width: 100px;height: 100px;" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">关键字</label>
                                    <div class="layui-input-inline">
                                        <input type="text" class="layui-input" name="keywords" value="{{$info['keywords']}}" maxlength="80" placeholder="请输入" autocomplete="off">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label"><span class="required">*</span>网页描述</label>
                                    <div class="layui-input-block">
                                        <textarea class="layui-textarea" name="description" maxlength="200" placeholder="200字以内" lay-verify="required">{{$info['description']}}</textarea>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">来源</label>
                                    <div class="layui-input-inline">
                                        <input type="text" class="layui-input" name="source" value="{{$info['source']}}" maxlength="60" placeholder="本站" autocomplete="off">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">排序</label>
                                    <div class="layui-input-inline">
                                        <input type="number" class="layui-input" name="sorting" value="{{$info['sorting']}}" axlength="10" placeholder="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label"><span class="required">*</span>是否显示</label>
                                    <div class="layui-input-inline">
                                        <input type="checkbox" name="status" @if($info['status'] == 1) checked="" @endif lay-skin="switch" lay-text="显示|不显示">
                                    </div>
                                </div>
                            </div>

                            <div class="layui-tab-item">
                                <div class="layui-form-pane">
                                    <div class="layui-form-item layui-form-text">
                                        <label class="layui-form-label"><span class="required">*</span>内容</label>
                                        <div class="layui-input-block">
                                            <textarea id="editor" class="layui-textarea" name="content" data-layedit_index="" data-folder="article">{{$info['content']}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id" value="{{$info['id']}}">

                    <div class="layui-form-item layui-layout-admin layui-hide">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button type="button" class="layui-btn layui-btn-primary close-btn">取消</button>
                                <button type="button" class="layui-btn laySave" lay-submit="" lay-filter="laySave">保存 <i class="layui-icon layui-icon-release"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" class="save_url" value="{{url(strtolower($model).'/edit/'.$info['id'])}}">
@endsection

@section('page_script')
    <script src="{{asset('static/admin/pages/js/editor.js')}}" type="text/javascript"></script>
    <script src="{{asset('static/admin/pages/js/upload_file.js')}}" type="text/javascript"></script>
    <script src="{{asset('static/admin/pages/js/article.js')}}" type="text/javascript"></script>
@endsection
