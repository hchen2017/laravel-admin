@extends('admin.layouts.iframe')

@section('content')
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    文章数
                    <span class="layui-badge layui-bg-blue layuiadmin-badge">总</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font">{{$data['totalArticle']}}</p>
                    <p>
                        总文章
                        <span class="layuiadmin-span-color"> <i class="layui-inline layui-icon layui-icon-read"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    文章数
                    <span class="layui-badge layui-bg-cyan layuiadmin-badge">月</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font">{{$data['monthArticle']}}</p>
                    <p>
                        本月文章
                        <span class="layuiadmin-span-color"> <i class="layui-inline layui-icon layui-icon-date"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    文章数
                    <span class="layui-badge layui-bg-green layuiadmin-badge">日</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">

                    <p class="layuiadmin-big-font">{{$data['todayArticle']}}</p>
                    <p>
                        今日文章
                        <span class="layuiadmin-span-color"> <i class="layui-inline layui-icon layui-icon-flag"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    会员数
                    <span class="layui-badge layui-bg-orange layuiadmin-badge">总</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">

                    <p class="layuiadmin-big-font">{{$data['totalUser']}}</p>
                    <p>
                        总会员
                        <span class="layuiadmin-span-color"> <i class="layui-inline layui-icon layui-icon-dialogue"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    会员数
                    <span class="layui-badge layui-bg-gray layuiadmin-badge">月</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">

                    <p class="layuiadmin-big-font">{{$data['monthUser']}}</p>
                    <p>
                        本月会员
                        <span class="layuiadmin-span-color"> <i class="layui-inline layui-icon layui-icon-dialogue"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    会员数
                    <span class="layui-badge layui-bg-red layuiadmin-badge">日</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">

                    <p class="layuiadmin-big-font">{{$data['todayUser']}}</p>
                    <p>
                        今日会员
                        <span class="layuiadmin-span-color"> <i class="layui-inline layui-icon layui-icon-dialogue"></i></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
