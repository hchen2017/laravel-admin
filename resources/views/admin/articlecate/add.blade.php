@extends('admin.layouts.iframe')

@section('content')
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body" style="padding: 15px;">
                <form class="layui-form" action="">
                    {!! csrf_field() !!}
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>名称</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input name" name="name" value="" data-name="" maxlength="20" lay-verify="required" placeholder="请输入" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label"><span class="required">*</span>短标题</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" name="sub_name" value="" maxlength="40" lay-verify="required" placeholder="请输入" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">封面图</label>
                        <div class="layui-input-block">
                            <div id="upload-file" class="upload_file-box">
                                <input type="hidden" class="layui-input cover_img" name="cover_img" value="">
                                <span id="upload-btn" class="upload-btn" data-multi="false" data-field="cover_img" data-total_count="1" data-count="0">
                                    <img class="load_img" src="{{asset('static/admin/img/default.png')}}" alt="图片上传" style="width: 100px;height: 100px;" />
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-inline">
                            <input type="number" class="layui-input" name="sorting" value="10" axlength="10" placeholder="" autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">是否启用</label>
                        <div class="layui-input-inline">
                            <input type="checkbox" name="status" checked="" lay-skin="switch" lay-text="启用|禁用">
                        </div>
                    </div>

                    <input type="hidden" name="id" value="">

                    <div class="layui-form-item layui-layout-admin layui-hide">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button type="button" class="layui-btn layui-btn-primary close-btn">取消</button>
                                <button type="button" class="layui-btn laySave" lay-submit="" lay-filter="laySave">保存 <i class="layui-icon layui-icon-release"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" class="save_url" value="{{url(strtolower($model).'/add')}}">
@endsection

@section('page_script')
    <script src="{{asset('static/admin/pages/js/upload_file.js')}}" type="text/javascript"></script>
    <script src="{{asset('static/admin/pages/js/articlecate.js')}}" type="text/javascript"></script>
@endsection
