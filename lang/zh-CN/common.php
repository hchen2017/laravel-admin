<?php
return [
    'Admin' => '管理员',
    'AdminRole' => '角色',
    'AdminLog' => '操作日志',
    'AdminMenu' => '菜单',
    'ArticleCate' => '文章分类',
    'Article' => '文章',
    'Member' => '会员',
    'User' => '会员',
    'File' => '文件',
    'Province' => '省市区',

    'VueAdmin' => '管理员',
    'VueAdminRole' => '角色',
    'VueAdminMenu' => '菜单',
];
