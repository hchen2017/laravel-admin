layui.use(['form', 'layer', 'table', 'laydate'], function () {

    var form = layui.form,
        //layer = parent.layer === undefined ? layui.layer : top.layer,
        layer = layui.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        table = layui.table;

    var datalist_url = $('.datalist_url').val();
    var delete_url = $('.delete_url').val();


    laydate.render({
        elem: '.lay-date-range',
        theme: 'gbw', // 主题
        type: 'datetime',
        range: '~', // '~' 自定义分割字符
        format: 'yyyy-MM-dd HH:mm:ss',
    });

    if ($('#dataTable').length > 0) {
        // Table 列表
        var tableIns = table.render({
            elem: '#dataTable',
            url: datalist_url,
            page: true,
            cellMinWidth: 95,
            //height : "full-100",
            limit: 20,
            limits: [10, 20, 30, 50, 100, 150, 200, 500],
            id: "tableList",
            cols: [[
                /*{type: "checkbox", fixed:"left", width:50},*/
                {field: 'id', title: '序号', width: 80, align: "center"},
                {field: 'username', title: '操作人', align: 'center'},
                {field: 'created_at', title: '操作时间', minWidth: 160, align: 'center'},
                {field: 'ip', title: 'IP地址', align: 'center'},
                {field: 'info', title: '日志内容', minWidth: 260, align: 'center'},
                /*{title: '操作', minWidth:175, templet:'#handleBar', fixed:"right", align:"center"},*/
                {
                    title: '操作', fixed: "right", align: "center", minWidth: 80, templet: function (data) {
                        var html = '';
                        html += '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del"><i class="layui-icon layui-icon-delete"></i>删除</a>';

                        return html;
                    }
                }
            ]]
        });

        // 搜索
        $(".search_btn").on("click", function () {
            var range_date = $('.searchBox').find('input[name=range_date]').val();
            if (range_date != '') {
                table.reload("tableList", {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                        range_date: range_date,
                    }
                });
            } else {
                layer.msg("请选择筛选条件");
            }
        });

        // 清空搜索
        $(".reset_btn").on("click", function () {
            table.reload("tableList", {
                where: {
                    range_date: '',
                }
            });
        });

        // 列表操作
        table.on('tool(dataTable)', function (obj) {
            var layEvent = obj.event,
                data = obj.data;
            // 删除
            if (layEvent === 'del') {
                layer.confirm('确定删除此数据？', {icon: 3, title: '提示信息'}, function (index) {
                    $.ajax({
                        url: delete_url + '/' + data.id,
                        data: {"_token": $('input[name=_token]').val()},
                        type: "POST",
                        dataType: "json",
                        success: function (res) {
                            if (res.code == 1) {
                                layer.msg('已删除', {icon: 1, time: 1000});
                                tableIns.reload();
                                layer.close(index);
                            } else {
                                layer.msg(res.msg);
                            }
                        },
                        error: function (data) {
                            layer.msg("服务器无响应");
                        }
                    });
                });
            }
        });
    }
});
