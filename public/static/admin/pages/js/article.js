layui.use(['form', 'layer', 'table', 'laydate', 'viewer'], function () {

    var form = layui.form,
        //layer = parent.layer === undefined ? layui.layer : top.layer,
        layer = layui.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        table = layui.table;

    var Viewer = layui.viewer;

    var datalist_url = $('.datalist_url').val();
    var add_url = $('.add_url').val();
    var edit_url = $('.edit_url').val();
    var delete_url = $('.delete_url').val();
    //var save_url = $('.save_url').val();


    // 日期
    laydate.render({
        elem: '.lay-date-range',
        type: 'datetime',
        range: '~', // '~' 自定义分割字符
        format: 'yyyy-MM-dd HH:mm:ss',
    });

    if ($('#dataTable').length > 0) {
        var viewer = '';
        var viewer_flag = false;

        // Table 列表
        var tableIns = table.render({
            elem: '#dataTable',
            url: datalist_url,
            page: true,
            cellMinWidth: 95,
            //height : "full-100",
            limit: 10,
            limits: [10, 20, 50, 100],
            id: "tableList",
            cols: [[
                /*{type: "checkbox", fixed:"left", width:50},*/
                /*{field: 'id', title: '编号', width: 80, align: "center"},*/
                {
                    field: 'cover_img', title: '封面图', minWidth: 110, align: "center", templet: function (data) {
                        if (data.cover_img) {
                            return '<a class="layer-photos" href="javascript:;"><img src="' + data.full_cover_img + '" layer-src="' + data.full_cover_img + '" style="height: 100%;" /></a>';
                        }
                        return '无';
                    }
                },
                {field: 'name', title: '名称', align: "center"},
                {field: 'cate_name', title: '分类', align: "center"},
                {field: 'keywords', title: '关键字', align: "center"},
                {field: 'sorting', title: '排序', minWidth: 120, align: "center"},
                {field: 'created_at', title: '创建时间', minWidth: 180, align: "center"},
                {
                    field: 'status', title: '是否显示', align: 'center', templet: function (data) {
                        var html = '';
                        switch (data.status) {
                            case 1:
                                html += '<button class="layui-btn layui-btn-normal layui-btn-xs">显示</button>';
                                break;
                            case 2:
                                html += '<button class="layui-btn layui-btn-warm layui-btn-xs">不显示</button>';
                                break;
                            default :
                                break;
                        }

                        return html;
                    }
                },
                {
                    title: '操作', fixed: "right", align: "center", minWidth: 160, templet: function (data) {
                        var html = '';
                        html += '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>';
                        html += '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del"><i class="layui-icon layui-icon-delete"></i>删除</a>';

                        return html;
                    }
                }
            ]],
            done: function (res, curr, count) {
                if (viewer_flag) {
                    viewer.destroy();
                }
                var table_body = document.getElementsByClassName('layui-table-body');
                var galley = table_body[0].getElementsByClassName('layui-table');
                //var galley = galley[0].getElementsByClassName('layer-photos');
                if (galley.length > 0) {
                    for (var i = 0; i < galley.length; i++) {
                        viewer = new Viewer(galley[i], {
                            url: 'layer-src'
                        });
                        viewer_flag = true;
                    }
                }
            }
        });

        // 搜索
        $(".search_btn").on("click", function () {
            var name = $('.searchBox').find('input[name=name]').val();
            var cate_id = $('.searchBox').find('select[name=cate_id]').val();
            var range_date = $('.searchBox').find('input[name=range_date]').val();
            if (name != '' || cate_id != '' || range_date != '') {
                table.reload("tableList", {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                        name: name,
                        cate_id: cate_id,
                        range_date: range_date
                    }
                });
            } else {
                layer.msg("请选择筛选条件");
            }
        });

        // 清空搜索
        $(".reset_btn").on("click", function () {
            table.reload("tableList", {
                where: {
                    name: '',
                    cate_id: '',
                    range_date: ''
                }
            });
        });

        $('.add_btn').on('click', function () {
            formHtml();
        });

        // 列表操作
        table.on('tool(dataTable)', function (obj) {
            var layEvent = obj.event,
                data = obj.data;
            // 删除
            if (layEvent === 'del') {
                layer.confirm('确定删除此数据？', {icon: 3, title: '提示信息'}, function (index) {
                    $.ajax({
                        url: delete_url + '/' + data.id,
                        data: {"_token": $('input[name=_token]').val()},
                        type: "POST",
                        dataType: "json",
                        success: function (res) {
                            if (res.code == 1) {
                                layer.msg('已删除', {icon: 1, time: 1000});
                                tableIns.reload();
                                layer.close(index);
                            } else {
                                layer.msg(res.msg);
                            }
                        },
                        error: function (data) {
                            layer.msg("服务器无响应");
                        }
                    });
                });
            } else if (layEvent === 'edit') {
                formHtml(data);
            }
        });

        // Form 表单
        function formHtml(data) {
            var content = add_url;

            if (data) {
                content = edit_url + '/' + data.id;
            }

            var index = layer.open({
                title: "文章",
                type: 2,
                maxmin: true,
                area: ["800px", "500px"],
                content: content,
                btn: ['确定', '取消'],
                success: function (layero, index) {
                    form.render();
                },
                yes: function (index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index],
                        submit = layero.find('iframe').contents().find('.laySave');

                    var $dom = layero.find('iframe').contents();
                    var save_url = $dom.find('.save_url').val();
                    var editor_index = $dom.find('#editor').attr('layedit_index');

                    // 监听提交
                    iframeWindow.layui.form.on('submit(laySave)', function (data) {
                        // 编辑器内容
                        //data.field.content = iframeWindow.layui.layedit.getContent(editor_index);
                        data.field.content = iframeWindow.layui.tinymce.get('#editor').getContent();
                        console.log(data.field.content);
                        if (!data.field.content) {
                            layer.msg('内容不能为空！', {icon: 5});
                            return false;
                        }

                        submitForm(data, save_url, table);
                    });

                    submit.trigger('click');
                }
            });
        }
    }

    // 自定义验证规则
    form.verify({
        cover_img: function (value) {
            if (value == '') {
                return '请上传封面图';
            }
        },
    });

    // 表单提交
    function submitForm(data, save_url, table) {
        if (data.field.status == 'on') {
            data.field.status = 1;
        } else {
            data.field.status = 2;
        }

        // 弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});

        $.ajax({
            url: save_url,
            data: data.field,
            type: "post",
            dataType: "json",
            success: function (res) {
                top.layer.msg(res.msg);
                if (res.code == 1) {
                    layer.closeAll("iframe");
                    table.reload('tableList', {});
                }
            },
            error: function (data) {
                layer.msg("服务器无响应");
            }
        });
    }
});
