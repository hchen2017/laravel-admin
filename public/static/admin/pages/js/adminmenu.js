layui.use(['form', 'layer'], function () {

    var form = layui.form,
        //layer = parent.layer === undefined ? layui.layer : top.layer,
        layer = layui.layer,
        $ = layui.jquery;


    var add_url = $('.add_url').val();
    var edit_url = $('.edit_url').val();
    var delete_url = $('.delete_url').val();
    var save_url = $('.save_url').val();
    var sorting_url = $('.sorting_url').val();

    var icons_url = $('.icons_url').val();


    $('.addChild_btn').on('click', function () {
        var url = add_url + '?parent_id=' + $(this).attr('data-id');
        formHtml(url);
    });
    $('.add_btn').on('click', function () {
        formHtml(add_url);
    });
    $('.edit_btn').on('click', function () {
        //var data = {};
        var url = edit_url + '/' + $(this).attr('data-id');
        formHtml(url);
    });
    $('.del_btn').on('click', function () {
        var data = {};
        data.id = $(this).attr('data-id');
        layer.confirm('确定删除此数据？', {icon: 3, title: '提示信息'}, function (index) {
            $.ajax({
                url: delete_url + '/' + data.id,
                data: {"_token": $('input[name=_token]').val()},
                type: "POST",
                dataType: "json",
                success: function (res) {
                    if (res.code == 1) {
                        layer.msg('已删除', {icon: 1, time: 1000});
                        layer.close(index);
                        window.location.reload();
                    } else {
                        layer.msg(res.msg);
                    }
                },
                error: function (data) {
                    layer.msg("服务器无响应");
                }
            });
        });
    });
    // 排序
    $('.sorting').on('blur', function () {
        var id = $(this).prev('.sorting_ids').val();
        var sorting = $(this).val();
        var sorting_ids = [id];
        var sorting = [sorting];
        var data = {
            '_token': $('input[name=_token]').val(),
            'sorting_ids': sorting_ids,
            'sorting': sorting
        };
        console.log(data);

        $.ajax({
            url: sorting_url,
            data: data,
            type: "POST",
            dataType: "json",
            success: function (res) {
                layer.msg(res.msg);
            },
            error: function (data) {
                layer.msg("服务器无响应");
            }
        });
    });

    // Form 表单
    function formHtml(url) {
        var content = url;

        var index = layer.open({
            title: "菜单管理",
            type: 2,
            maxmin: true,
            area: ["800px", "600px"],
            content: content,
            success: function (layero, index) {
                form.render();
            }
        });
    }

    // 选择图标
    $('.select-icon').on('click', function () {
        var content = icons_url;

        var index = layer.open({
            title: "图标管理",
            type: 2,
            area: ["600px", "500px"],
            content: content,
            success: function (layero, index) {
                // 选中图标
                layero.find('iframe').contents().find('.doc-icon li').on('click', function () {
                    var class_name = $(this).find('i').attr('data-class');
                    console.log(class_name);

                    if (class_name) {
                        $('.icon_name').val(class_name);
                        $('.icon_design').find('i').removeClass().addClass('layui-icon ' + class_name);

                        layer.msg('选择成功', {icon: 1, time: 1000});
                    }

                    layer.close(index);
                });
            }
        });
    });
    // 删除图标
    $('.del-icon').on('click', function () {
        $('.icon_name').val('');
        $('.icon_design').find('i').removeClass();
    });
    $('.icon_name').on('change', function () {
        $('.icon_design').find('i').removeClass();
    });

    // 表单提交
    form.on("submit(laySave)", function (data) {
        submitForm(data);
    });

    // 表单提交
    function submitForm(data) {
        if (data.field.status == 'on') {
            data.field.status = 1;
        } else {
            data.field.status = 2;
        }

        // 弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: save_url,
            data: data.field,
            type: "post",
            dataType: "json",
            success: function (res) {
                top.layer.msg(res.msg);
                if (res.code == 1) {
                    layer.closeAll("iframe");
                    parent.location.reload();
                }
            },
            error: function (data) {
                layer.msg("服务器无响应");
            }
        });
    }


    $(document).on('click', '.open-btn', function () {
        tree_open($(this));
    });

    $(document).on('click', '.open-child-btn', function () {
        rowClicked($(this));
    });

    // 展开收缩
    function tree_open(obj) {
        var tree = $('#list-table tr[id^="2_"], #list-table tr[id^="3_"], #list-table tr[id^="4_"], #list-table tr[id^="5_"], #list-table tr[id^="6_"]'); //'table-row'，支持6级
        if (tree.css('display') == 'table-row') {
            $(obj).html('<i class="layui-icon layui-icon-down layuiadmin-button-btn"></i>展开');
            tree.css('display', 'none');
            $("span[id^='icon_']").removeClass('layui-icon-triangle-d');
            $("span[id^='icon_']").addClass('layui-icon-triangle-r');
        } else {
            $(obj).html('<i class="layui-icon layui-icon-up layuiadmin-button-btn"></i>收缩');
            tree.css('display', 'table-row');
            $("span[id^='icon_']").addClass('layui-icon-triangle-d');
            $("span[id^='icon_']").removeClass('layui-icon-triangle-r');
        }
    }

    // 展开/收缩 下级
    function rowClicked(obj) {
        obj = obj[0]; // obj['context']
        var span = obj;
        obj = obj.parentNode.parentNode;
        var tbl = document.getElementById("list-table");
        var lvl = parseInt(obj.className);
        var fnd = false;
        var sub_display = $(span).hasClass('layui-icon-triangle-d') ? 'none' : '' ? 'block' : 'table-row';
        //console.log(sub_display);
        if (sub_display == 'none') {
            $(span).removeClass('layui-icon-triangle-d');
            $(span).addClass('layui-icon-triangle-r');//btn-warning
        } else {
            $(span).removeClass('layui-icon-triangle-r');
            $(span).addClass('layui-icon-triangle-d');
        }

        for (i = 0; i < tbl.rows.length; i++) {
            var row = tbl.rows[i];
            if (row == obj) {
                fnd = true;
            } else {
                if (fnd == true) {
                    var cur = parseInt(row.className);
                    var icon = 'icon_' + row.id;
                    if (cur > lvl) {
                        row.style.display = sub_display;
                        if (sub_display != 'none') {
                            var iconimg = document.getElementById(icon);
                            $(iconimg).removeClass('layui-icon-triangle-r');
                            $(iconimg).addClass('layui-icon-triangle-d');
                        } else {
                            $(iconimg).removeClass('layui-icon-triangle-d');
                            $(iconimg).addClass('layui-icon-triangle-r');
                        }
                    } else {
                        fnd = false;
                        break;
                    }
                }
            }
        }

        for (i = 0; i < obj.cells[0].childNodes.length; i++) {
            var imgObj = obj.cells[0].childNodes[i];
            if (imgObj.tagName == "IMG") {
                if ($(imgObj).hasClass('layui-icon-triangle-r')) {
                    $(imgObj).removeClass('layui-icon-triangle-r');
                    $(imgObj).addClass('layui-icon-triangle-d');
                } else {
                    $(imgObj).removeClass('layui-icon-triangle-d');
                    $(imgObj).addClass('layui-icon-triangle-r');
                }
            }
        }
    }
});
