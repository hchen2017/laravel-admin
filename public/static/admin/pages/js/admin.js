layui.use(['form', 'layer', 'table', 'laydate', 'viewer'], function () {

    var form = layui.form,
        //layer = parent.layer === undefined ? layui.layer : top.layer,
        layer = layui.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        table = layui.table;

    var Viewer = layui.viewer;

    var datalist_url = $('.datalist_url').val();
    var add_url = $('.add_url').val();
    var edit_url = $('.edit_url').val();
    var delete_url = $('.delete_url').val();
    var check_phone_url = $('.check_phone_url').val();
    //var save_url = $('.save_url').val();


    // 日期
    laydate.render({
        elem: '.lay-date-range',
        type: 'datetime',
        range: '~', // '~' 自定义分割字符
        format: 'yyyy-MM-dd HH:mm:ss',
    });

    if ($('#dataTable').length > 0) {
        // Table 列表
        var tableIns = table.render({
            elem: '#dataTable',
            url: datalist_url,
            page: true,
            cellMinWidth: 95,
            //height : "full-100",
            limit: 10,
            limits: [10, 20, 50, 100],
            id: "tableList",
            cols: [[
                /*{type: "checkbox", fixed:"left", width:50},*/
                /*{field: 'id', title: '编号', width: 80, align: "center"},*/
                {field: 'name', title: '昵称', align: "center"},
                {field: 'phone', title: '手机号', minWidth: 120, align: "center"},
                {field: 'role_names', title: '角色', minWidth: 200, align: 'center'},
                {
                    title: '操作', fixed: "right", align: "center", minWidth: 160, templet: function (data) {
                        var html = '';
                        if (data.type != 0) {
                            html += '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>';
                            html += '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del"><i class="layui-icon layui-icon-delete"></i>删除</a>';
                        }

                        return html;
                    }
                }
            ]],
            done: function (res, curr, count) {
                //
            }
        });

        // 搜索
        $(".search_btn").on("click", function () {
            var name = $('.searchBox').find('input[name=name]').val();
            var range_date = $('.searchBox').find('input[name=range_date]').val();
            if (name != '' || range_date != '') {
                table.reload("tableList", {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                        name: name,
                        range_date: range_date
                    }
                });
            } else {
                layer.msg("请选择筛选条件");
            }
        });

        // 清空搜索
        $(".reset_btn").on("click", function () {
            table.reload("tableList", {
                where: {
                    name: '',
                    range_date: ''
                }
            });
        });

        $('.add_btn').on('click', function () {
            formHtml();
        });

        // 列表操作
        table.on('tool(dataTable)', function (obj) {
            var layEvent = obj.event,
                data = obj.data;
            // 删除
            if (layEvent === 'del') {
                layer.confirm('确定删除此数据？', {icon: 3, title: '提示信息'}, function (index) {
                    $.ajax({
                        url: delete_url + '/' + data.id,
                        data: {"_token": $('input[name=_token]').val()},
                        type: "POST",
                        dataType: "json",
                        success: function (res) {
                            if (res.code == 1) {
                                layer.msg('已删除', {icon: 1, time: 1000});
                                tableIns.reload();
                                layer.close(index);
                            } else {
                                layer.msg(res.msg);
                            }
                        },
                        error: function (data) {
                            layer.msg("服务器无响应");
                        }
                    });
                });
            } else if (layEvent === 'edit') {
                formHtml(data);
            }
        });

        // Form 表单
        function formHtml(data) {
            var content = add_url;

            if (data) {
                content = edit_url + '/' + data.id;
            }

            var index = layer.open({
                title: "管理员管理",
                type: 2,
                maxmin: true,
                area: ["600px", "500px"],
                content: content,
                btn: ['确定', '取消'],
                success: function (layero, index) {
                    form.render();
                },
                yes: function (index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index],
                        submit = layero.find('iframe').contents().find('.laySave');

                    var save_url = layero.find('iframe').contents().find('.save_url').val();
                    // 监听提交
                    iframeWindow.layui.form.on('submit(laySave)', function (data) {
                        var password = data.field.password;
                        var confirm_password = data.field.confirm_password;

                        if (password) {
                            if (password != confirm_password) {
                                layer.msg('两次密码不一致！');
                                $('.confirm_password').focus();
                                return false;
                            }
                        }

                        var role_ids_len = layero.find('iframe').contents().find("input[name^='role_ids']:checked").length;
                        if (role_ids_len <= 0) {
                            layer.msg('请至少选择一个角色！', {icon: 5});
                            return false;
                        }

                        submitForm(data, save_url, table);
                    });

                    submit.trigger('click');
                }
            });
        }
    }

    // 自定义验证规则
    form.verify({
        phone: function (value) {
            if (value == '') {
                return '请填写手机号';
            } else {
                if (!verify_data(value, 'phone')) {
                    return '手机号格式不正确';
                }
            }
        },
        password: function (value) {
            var id = $('input[name=id]').val();
            if (!id) {
                if (value == '') {
                    return '请填写密码';
                }
                if (!/^[\w]{6,12}$/.test(value)) {
                    return '密码格式不正确，应为6-12位数字或字母组成';
                }
            }
        },
        confirm_password: function (value) {
            var id = $('input[name=id]').val();
            if (!id) {
                if (value == '') {
                    return '请确认密码';
                }
            }
        },
    });

    // 表单提交
    function submitForm(data, save_url, table) {
        if (data.field.status == 'on') {
            data.field.status = 1;
        } else {
            data.field.status = 2;
        }

        // 弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});

        $.ajax({
            url: save_url,
            data: data.field,
            type: "post",
            dataType: "json",
            success: function (res) {
                top.layer.msg(res.msg);
                if (res.code == 1) {
                    layer.closeAll("iframe");
                    table.reload('tableList', {});
                }
            },
            error: function (data) {
                layer.msg("服务器无响应");
            }
        });
    }
});
