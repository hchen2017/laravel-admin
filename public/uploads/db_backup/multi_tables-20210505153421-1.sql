-- -----------------------------
-- Think MySQL Data Transfer 
-- 
-- Host     : 127.0.0.1
-- Port     : 20971520
-- Database : laravel
-- 
-- Part : #1
-- Date : 2021-05-05 15:34:21
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------
-- Table structure for `la_admin_logs`
-- -----------------------------
DROP TABLE IF EXISTS `la_admin_logs`;
CREATE TABLE `la_admin_logs` (
  `id` bigint(16) unsigned NOT NULL AUTO_INCREMENT,
  `created_id` int(11) DEFAULT '0',
  `created_at` int(11) DEFAULT '0',
  `updated_id` int(11) DEFAULT '0',
  `updated_at` int(11) DEFAULT '0',
  `deleted_id` int(11) DEFAULT '0',
  `deleted_at` int(11) DEFAULT '0',
  `admin_id` int(11) NOT NULL DEFAULT '1' COMMENT '管理员ID',
  `info` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '操作详细',
  `ip` varchar(30) DEFAULT NULL COMMENT '操作时的ip',
  `url` varchar(80) DEFAULT NULL COMMENT '日志访问的地址',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '状态 1：显示，2：隐藏，90：删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='管理员操作日志表';

-- -----------------------------
-- Records of `la_admin_logs`
-- -----------------------------
INSERT INTO `la_admin_logs` VALUES ('1', '1', '1620183623', '0', '1620183623', '0', '0', '1', 'Admin更新了一条ArticleCate数据。', '127.0.0.1', '\\App\\Http\\Controllers\\Admin\\ArticleCateController@edit', '1');
INSERT INTO `la_admin_logs` VALUES ('2', '1', '1620186290', '0', '1620186290', '0', '0', '1', '管理员登录', '127.0.0.1', 'App\\Http\\Controllers\\Admin\\AuthController@authenticate', '1');
INSERT INTO `la_admin_logs` VALUES ('4', '1', '1620193556', '0', '1620193556', '0', '0', '1', 'Admin删除了一条AdminLog数据。', '127.0.0.1', '\\App\\Http\\Controllers\\Admin\\AdminLogController@delete', '1');

-- -----------------------------
-- Table structure for `la_admin_menu`
-- -----------------------------
DROP TABLE IF EXISTS `la_admin_menu`;
CREATE TABLE `la_admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_id` int(10) NOT NULL DEFAULT '0',
  `created_at` int(10) NOT NULL DEFAULT '0',
  `updated_id` int(10) NOT NULL DEFAULT '0',
  `updated_at` int(10) NOT NULL DEFAULT '0',
  `deleted_id` int(10) NOT NULL DEFAULT '0',
  `deleted_at` int(10) DEFAULT '0',
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `app` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '应用名',
  `controller` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '控制器名',
  `action` varchar(100) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '操作名称',
  `route` varchar(100) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '路由',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型 1=模块 2=列表 3=操作',
  `sorting` int(10) NOT NULL DEFAULT '0',
  `target` varchar(20) COLLATE utf8_unicode_ci DEFAULT '_self',
  `icon` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `access` tinyint(4) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1：显示 2：不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='后台菜单表';

-- -----------------------------
-- Records of `la_admin_menu`
-- -----------------------------
INSERT INTO `la_admin_menu` VALUES ('1', '1', '1461766608', '1', '1620183969', '0', '0', '0', '控制台', 'admin', 'Index', 'dashboard', 'dashboard', '1', '0', '_self', 'layui-icon-home', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('2', '1', '1461766608', '1', '1620193599', '0', '0', '0', '用户管理', '', '', '', '', '1', '5', '_self', 'layui-icon-read', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('7', '1', '1464080269', '1', '1620193600', '0', '0', '0', '内容管理', '', '', '', '', '1', '90', '_self', 'layui-icon-group', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('8', '1', '1464080269', '1', '1620193633', '0', '0', '0', '菜单管理', '', '', '', '', '1', '95', '_self', 'layui-icon-list', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('10', '1', '1462523669', '1', '1463116908', '0', '0', '0', '设置', '', '', '', '', '1', '100', '_self', 'layui-icon-set', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('11', '1', '1461766608', '1', '1462533871', '0', '0', '0', '个人中心', 'admin', 'Admin', 'profile', 'profile', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('12', '1', '1461766608', '1', '1462546137', '0', '0', '0', '重置密码', 'admin', 'Admin', 'resetPassword', 'resetPassword', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('13', '1', '1464080269', '1', '1609129439', '0', '0', '8', '菜单列表', 'admin', 'AdminMenu', 'index', 'adminmenu', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('14', '1', '1461766608', '1', '1608775493', '0', '0', '13', '添加', 'admin', 'AdminMenu', 'add', 'adminmenu/add', '3', '1', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('15', '1', '1461766608', '1', '1608775501', '0', '0', '13', '编辑', 'admin', 'AdminMenu', 'edit', 'adminmenu/edit/{id}', '3', '2', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('16', '1', '1461766608', '1', '1608775506', '0', '0', '13', '删除', 'admin', 'AdminMenu', 'delete', 'adminmenu/delete/{id}', '3', '3', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('17', '1', '1464080269', '1', '1478192928', '0', '0', '2', '管理组', '', '', '', 'articlecate/delete/{id}', '1', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('18', '1', '1464080269', '1', '1620193889', '0', '0', '17', '角色管理', 'admin', 'AdminRole', 'index', 'adminrole', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('19', '1', '1464080269', '1', '1620193887', '0', '0', '18', '添加', 'admin', 'AdminRole', 'add', 'adminrole/add', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('20', '1', '1464080269', '1', '1478192928', '0', '0', '18', '编辑', 'admin', 'AdminRole', 'edit', 'adminrole/edit/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('21', '1', '1464080269', '1', '1478192928', '0', '0', '18', '删除', 'admin', 'AdminRole', 'delete', 'adminrole/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('22', '1', '1462523752', '1', '1484056560', '0', '0', '17', '管理员', 'admin', 'Admin', 'index', 'admin', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('23', '1', '1462525036', '1', '1462525058', '0', '0', '22', '添加', 'admin', 'Admin', 'add', 'admin/add', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('24', '1', '1462525089', '1', '1464932528', '0', '0', '22', '编辑', 'admin', 'Admin', 'edit', 'admin/edit/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('25', '1', '1462525163', '1', '1462525170', '0', '0', '22', '删除', 'admin', 'Admin', 'delete', 'admin/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('26', '1', '1464080269', '1', '1478192928', '0', '0', '2', '日志管理', 'admin', 'AdminLog', 'index', 'adminlog', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('27', '1', '1464080269', '1', '1478192928', '0', '0', '26', '删除', 'admin', 'AdminLog', 'delete', 'adminlog/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('28', '1', '1464080269', '1', '1478192938', '0', '0', '7', '文章分类', 'admin', 'ArticleCate', 'index', 'articlecate', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('29', '1', '1464080269', '1', '1478192928', '0', '0', '28', '添加', 'admin', 'ArticleCate', 'add', 'articlecate/add', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('30', '1', '1464080269', '1', '1478192928', '0', '0', '28', '编辑', 'admin', 'ArticleCate', 'edit', 'articlecate/edit/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('31', '1', '1464080269', '1', '1478192928', '0', '0', '28', '删除', 'admin', 'ArticleCate', 'delete', 'articlecate/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('32', '1', '1464080269', '1', '1478192938', '0', '0', '7', '文章列表', 'admin', 'Article', 'index', 'article', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('33', '1', '1464080269', '1', '1478192928', '0', '0', '32', '添加', 'admin', 'Article', 'add', 'article/add', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('34', '1', '1464080269', '1', '1478192928', '0', '0', '32', '编辑', 'admin', 'Article', 'edit', 'article/edit/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('35', '1', '1464080269', '1', '1478192928', '0', '0', '32', '删除', 'admin', 'Article', 'delete', 'article/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('36', '1', '1464080269', '1', '1478192928', '0', '0', '32', '导出', 'admin', 'Article', 'export', 'article/export', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('37', '1', '1608775353', '1', '1608775539', '0', '0', '13', '排序', 'admin', 'AdminMenu', 'listOrders', 'adminmenu/listOrders', '3', '10', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('38', '1', '1464080269', '1', '1478192928', '0', '0', '10', '数据库管理', '', '', '', '', '1', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('39', '1', '1464080269', '1', '1478192928', '0', '0', '38', '数据库备份', 'admin', 'Database', 'export', 'database/export', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('40', '1', '1464080269', '1', '1478192928', '0', '0', '39', '备份提交', 'admin', 'Database', 'exportPost', 'database/exportPost', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('41', '1', '1464080269', '1', '1478192928', '0', '0', '38', '数据库还原', 'admin', 'Database', 'import', 'database/import', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('42', '1', '1464080269', '1', '1478192928', '0', '0', '41', '还原提交', 'admin', 'Database', 'importPost', 'database/importPost', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('43', '1', '1464080269', '1', '1478192928', '0', '0', '39', '优化表', 'admin', 'Database', 'optimize', 'database/optimize', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('44', '1', '1464080269', '1', '1478192928', '0', '0', '39', '修复表', 'admin', 'Database', 'repair', 'database/repair', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('45', '1', '1464080269', '1', '1478192928', '0', '0', '38', '删除备份文件', 'admin', 'Database', 'delete', 'database/delete', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('46', '1', '1464080269', '1', '1478192928', '0', '0', '38', '下载', 'admin', 'Database', 'download', 'database/download', '3', '0', '_self', '', '0', '1');

-- -----------------------------
-- Table structure for `la_admin_role_user`
-- -----------------------------
DROP TABLE IF EXISTS `la_admin_role_user`;
CREATE TABLE `la_admin_role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `group_id` (`role_id`) USING BTREE,
  KEY `user_id` (`admin_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='管理员角色对应表';

-- -----------------------------
-- Records of `la_admin_role_user`
-- -----------------------------
INSERT INTO `la_admin_role_user` VALUES ('1', '1', '1');
INSERT INTO `la_admin_role_user` VALUES ('2', '2', '1');
INSERT INTO `la_admin_role_user` VALUES ('3', '3', '1');
INSERT INTO `la_admin_role_user` VALUES ('5', '3', '3');
INSERT INTO `la_admin_role_user` VALUES ('7', '1', '2');

-- -----------------------------
-- Table structure for `la_admin_roles`
-- -----------------------------
DROP TABLE IF EXISTS `la_admin_roles`;
CREATE TABLE `la_admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_id` int(10) DEFAULT '0',
  `created_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_id` int(10) DEFAULT '0',
  `updated_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `deleted_id` int(10) DEFAULT '0',
  `deleted_at` int(255) DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT '0' COMMENT '上级ID',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `access` text COMMENT '用户组拥有的规则ID，多个规则 , 隔开（rules）',
  `sorting` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1：正常，2：禁用',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parentId` (`parent_id`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='后台角色表';

-- -----------------------------
-- Records of `la_admin_roles`
-- -----------------------------
INSERT INTO `la_admin_roles` VALUES ('1', '0', '1521983887', '1', '1531665598', '0', '0', '0', '超级管理员', '拥有网站最高管理员权限！', '1,2,3,5,7,160,159,166,172,179,178,181,183,10,9,13,12,154,155,14,15,22,39,16,184,41,210,42,48,54,60,66,69,70,71,72,74,73,76,75,77,79,78,80,82,81,83,84,86,85,88,87,158,91,90,89,93,92,97,96,95,94,99,98,100,101,105,104,103,102,107,106,108,111,110,109,113,112,114,115,118,117,116,120,119,121,122,123,267,266,265,269,268,270,272,271,126,125,124,128,127,129,131,130,132,133,275,274,273,277,276,278,281,280,279,283,282,284,138,136,135,134,137,139,142,141,140,144,143,145,148,147,146,150,149,151,187,186,185,189,188,190,192,191,194,193,195,196,202,216,217,226,229,232,234,237,236,235,239,238,240,243,242,241,245,244,246,249,248,247,251,250,252,255,254,253,257,256,258,261,260,259,263,262,264,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,', '1', '1');
INSERT INTO `la_admin_roles` VALUES ('2', '0', '1521983887', '1', '1609128540', '0', '0', '0', '分类管理员', '权限由最高管理员分配！', '1,12,2,25,26,27,28,29,30,31,32,33,7,17,18,19,20,21,22,23,24,8,13,14', '15', '2');
INSERT INTO `la_admin_roles` VALUES ('3', '0', '1521983887', '1', '1604362820', '0', '0', '0', '文章管理员', '权限由最高管理员分配！', '1,7,2,10,9,12,14,23,22,31,39,15,42,41,48,54,60,66,70,69,71,72,78,85,84,87,89,95,103,102,17,16,20', '20', '1');

-- -----------------------------
-- Table structure for `la_admins`
-- -----------------------------
DROP TABLE IF EXISTS `la_admins`;
CREATE TABLE `la_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_id` int(10) NOT NULL DEFAULT '0',
  `created_at` int(10) NOT NULL DEFAULT '0',
  `updated_id` int(10) NOT NULL DEFAULT '0',
  `updated_at` int(10) NOT NULL DEFAULT '0',
  `deleted_id` int(10) NOT NULL DEFAULT '0',
  `deleted_at` int(10) DEFAULT '0',
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apikey` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `auto_logout` int(10) NOT NULL DEFAULT '15',
  `last_login_time` int(10) DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '最后登录ip',
  `type` tinyint(1) DEFAULT '2' COMMENT '类型 1=超级管理员 2=普通管理员',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='管理员表';

-- -----------------------------
-- Records of `la_admins`
-- -----------------------------
INSERT INTO `la_admins` VALUES ('1', '0', '1461766664', '1', '1620186412', '0', '', 'Admin', 'admin', '$2y$10$loJxi.dSj/JmdKqkEKYuVOE5hTxP5GOyZgCaFEfpMBsrAx8YBF3Ly', '12378945', 'TUv3rNye9Pgq0Er0gURmWT0uTbtOn2Sr217mIMBdimrO5YvQE0JOrNXiix6V', '1df67884df69f2f0a5725aef243839ac', '15', '1620186412', '127.0.0.1', '1', '0');
INSERT INTO `la_admins` VALUES ('2', '1', '1608196877', '1', '1609128142', '0', '0', 'test', '', '$2y$10$JgMW6/EFSsG.rozV6xettO.vV17z7lXTUb0phRxcssLYhvg03bMLm', '13311111111', '', '', '15', '', '', '2', '1');

-- -----------------------------
-- Table structure for `la_article_cate`
-- -----------------------------
DROP TABLE IF EXISTS `la_article_cate`;
CREATE TABLE `la_article_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_id` int(10) NOT NULL DEFAULT '0',
  `created_at` int(10) NOT NULL DEFAULT '0',
  `updated_id` int(10) NOT NULL DEFAULT '0',
  `updated_at` int(10) NOT NULL DEFAULT '0',
  `deleted_id` int(10) NOT NULL DEFAULT '0',
  `deleted_at` int(10) DEFAULT '0',
  `parent_id` int(10) DEFAULT '0' COMMENT '上级ID',
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `sub_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '副标题',
  `icon` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'icon图标',
  `cover_img` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '封面图',
  `sorting` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 1=显示 2=不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='文章分类表';

-- -----------------------------
-- Records of `la_article_cate`
-- -----------------------------
INSERT INTO `la_article_cate` VALUES ('1', '1', '1461766664', '1', '1608519330', '1', '1608519330', '0', '功能更新', '系统代码每日更新每周发布', '', '', '10', '1');
INSERT INTO `la_article_cate` VALUES ('2', '1', '1608188451', '1', '1608519309', '1', '1608519309', '0', '运营知识', '互联网运营知识分享', '', 'http://img.xiaolekeji.cn/5fdd97d5e1801.png', '10', '1');
INSERT INTO `la_article_cate` VALUES ('3', '1', '1608188911', '1', '1608519459', '1', '1608519208', '0', '行业动态', '最新互联网消息', '', 'http://img.xiaolekeji.cn/5fdda2012a4f4.png', '10', '1');
INSERT INTO `la_article_cate` VALUES ('4', '1', '1608365300', '1', '1620183623', '1', '1608519190', '0', '11', '111', '', '', '10', '2');

-- -----------------------------
-- Table structure for `la_articles`
-- -----------------------------
DROP TABLE IF EXISTS `la_articles`;
CREATE TABLE `la_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_id` int(10) NOT NULL DEFAULT '0',
  `created_at` int(10) NOT NULL DEFAULT '0',
  `updated_id` int(10) NOT NULL DEFAULT '0',
  `updated_at` int(10) NOT NULL DEFAULT '0',
  `deleted_id` int(10) NOT NULL DEFAULT '0',
  `deleted_at` int(10) DEFAULT '0',
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `cate_id` int(10) DEFAULT NULL COMMENT '分类ID',
  `cover_img` varchar(120) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '封面图',
  `source` varchar(60) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '来源',
  `keywords` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '关键字',
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `content` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '内容',
  `count_views` int(10) DEFAULT NULL COMMENT '浏览量',
  `sorting` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 1=显示 2=不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='文章表';

-- -----------------------------
-- Records of `la_articles`
-- -----------------------------
INSERT INTO `la_articles` VALUES ('1', '1', '1461766664', '1', '1608192674', '0', '0', '测试文章', '1', '', '', '关键字', '描述', '文章内容123', '', '15', '1');
INSERT INTO `la_articles` VALUES ('2', '1', '1608192653', '1', '1608884104', '0', '0', '测试文章002', '3', 'http://img.xiaolekeji.cn/5fdd69f908437.png', '', '', '文章描述', '<p>&nbsp; &nbsp; 这里是内容</p>\n<pre class=\"language-markup\"><code>{\n	\"code\": 200,\n	\"msg\": \"成功\",\n	\"data\": [\n		{\n			\"unitPrice\": 150,\n			\"materialUrl\": \"http://item.jd.com/100008969320.html\",\n			\"endDate\": 32472115200000,\n			\"isFreeFreightRisk\": 0,\n			\"isFreeShipping\": 2,\n			\"commisionRatioWl\": 2,\n			\"commisionRatioPc\": 2,\n			\"imgUrl\": \"http://img14.360buyimg.com/n1/jfs/t1/133781/23/5967/64052/5f27dcf9Ee5efcfbc/dd5f42a6c5dceeae.jpg\",\n			\"vid\": 1000308441,\n			\"cidName\": \"母婴\",\n			\"wlUnitPrice\": 150,\n			\"cid2Name\": \"营养辅食\",\n			\"isSeckill\": 0,\n			\"cid2\": 1524,\n			\"cid3Name\": \"DHA\",\n			\"inOrderCount\": 8343,\n			\"cid3\": 7055,\n			\"shopId\": 1000308441,\n			\"isJdSale\": 1,\n			\"goodsName\": \"爷爷的农场法国原装进口食用油DHA营养核桃油250ml\",\n			\"skuId\": 100008969320,\n			\"startDate\": 1597248000000,\n			\"cid\": 1319\n		}\n	]\n}</code></pre>\n<table style=\"border-collapse: collapse; width: 100%;\" border=\"1\">\n<tbody>\n<tr>\n<td style=\"width: 47.3699%;\">&nbsp;</td>\n<td style=\"width: 47.5479%;\">&nbsp;</td>\n</tr>\n<tr>\n<td style=\"width: 47.3699%;\">&nbsp;</td>\n<td style=\"width: 47.5479%;\">&nbsp;</td>\n</tr>\n</tbody>\n</table>\n<p><img src=\"http://img.xiaolekeji.cn/5fe43b3b61fa1.png\" width=\"491\" height=\"270\" /></p>\n<p>这里还是内容，这里还是内容。</p>', '', '10', '1');
INSERT INTO `la_articles` VALUES ('3', '1', '1608345961', '1', '1608684333', '0', '0', '测试003', '3', 'http://img.xiaolekeji.cn/5fdd6942d49b1.png', '本站', '', '描述', '<img src=\"http://img.xiaolekeji.cn/5fdd694c73299.png\" alt=\"\"><img src=\"http://img.xiaolekeji.cn/5fdd6951b04d2.png\" alt=\"\"><img src=\"http://manage.xlkj.cn/static/plugins/layui/images/face/63.gif\" alt=\"[给力]\">', '', '10', '1');
