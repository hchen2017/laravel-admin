-- -----------------------------
-- Think MySQL Data Transfer 
-- 
-- Host     : 127.0.0.1
-- Port     : 20971520
-- Database : laravel
-- 
-- Part : #1
-- Date : 2021-05-05 15:41:17
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------
-- Table structure for `la_admin_logs`
-- -----------------------------
DROP TABLE IF EXISTS `la_admin_logs`;
CREATE TABLE `la_admin_logs` (
  `id` bigint(16) unsigned NOT NULL AUTO_INCREMENT,
  `created_id` int(11) DEFAULT '0',
  `created_at` int(11) DEFAULT '0',
  `updated_id` int(11) DEFAULT '0',
  `updated_at` int(11) DEFAULT '0',
  `deleted_id` int(11) DEFAULT '0',
  `deleted_at` int(11) DEFAULT '0',
  `admin_id` int(11) NOT NULL DEFAULT '1' COMMENT '管理员ID',
  `info` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '操作详细',
  `ip` varchar(30) DEFAULT NULL COMMENT '操作时的ip',
  `url` varchar(80) DEFAULT NULL COMMENT '日志访问的地址',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '状态 1：显示，2：隐藏，90：删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='管理员操作日志表';

-- -----------------------------
-- Records of `la_admin_logs`
-- -----------------------------
INSERT INTO `la_admin_logs` VALUES ('1', '1', '1620183623', '0', '1620183623', '0', '0', '1', 'Admin更新了一条ArticleCate数据。', '127.0.0.1', '\\App\\Http\\Controllers\\Admin\\ArticleCateController@edit', '1');
INSERT INTO `la_admin_logs` VALUES ('2', '1', '1620186290', '0', '1620186290', '0', '0', '1', '管理员登录', '127.0.0.1', 'App\\Http\\Controllers\\Admin\\AuthController@authenticate', '1');
INSERT INTO `la_admin_logs` VALUES ('4', '1', '1620193556', '0', '1620193556', '0', '0', '1', 'Admin删除了一条AdminLog数据。', '127.0.0.1', '\\App\\Http\\Controllers\\Admin\\AdminLogController@delete', '1');
