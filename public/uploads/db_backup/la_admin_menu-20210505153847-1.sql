-- -----------------------------
-- Think MySQL Data Transfer 
-- 
-- Host     : 127.0.0.1
-- Port     : 20971520
-- Database : laravel
-- 
-- Part : #1
-- Date : 2021-05-05 15:38:47
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------
-- Table structure for `la_admin_menu`
-- -----------------------------
DROP TABLE IF EXISTS `la_admin_menu`;
CREATE TABLE `la_admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_id` int(10) NOT NULL DEFAULT '0',
  `created_at` int(10) NOT NULL DEFAULT '0',
  `updated_id` int(10) NOT NULL DEFAULT '0',
  `updated_at` int(10) NOT NULL DEFAULT '0',
  `deleted_id` int(10) NOT NULL DEFAULT '0',
  `deleted_at` int(10) DEFAULT '0',
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `app` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '应用名',
  `controller` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '控制器名',
  `action` varchar(100) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '操作名称',
  `route` varchar(100) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '路由',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型 1=模块 2=列表 3=操作',
  `sorting` int(10) NOT NULL DEFAULT '0',
  `target` varchar(20) COLLATE utf8_unicode_ci DEFAULT '_self',
  `icon` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `access` tinyint(4) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1：显示 2：不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='后台菜单表';

-- -----------------------------
-- Records of `la_admin_menu`
-- -----------------------------
INSERT INTO `la_admin_menu` VALUES ('1', '1', '1461766608', '1', '1620183969', '0', '0', '0', '控制台', 'admin', 'Index', 'dashboard', 'dashboard', '1', '0', '_self', 'layui-icon-home', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('2', '1', '1461766608', '1', '1620193599', '0', '0', '0', '用户管理', '', '', '', '', '1', '5', '_self', 'layui-icon-read', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('7', '1', '1464080269', '1', '1620193600', '0', '0', '0', '内容管理', '', '', '', '', '1', '90', '_self', 'layui-icon-group', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('8', '1', '1464080269', '1', '1620193633', '0', '0', '0', '菜单管理', '', '', '', '', '1', '95', '_self', 'layui-icon-list', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('10', '1', '1462523669', '1', '1463116908', '0', '0', '0', '设置', '', '', '', '', '1', '100', '_self', 'layui-icon-set', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('11', '1', '1461766608', '1', '1462533871', '0', '0', '0', '个人中心', 'admin', 'Admin', 'profile', 'profile', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('12', '1', '1461766608', '1', '1462546137', '0', '0', '0', '重置密码', 'admin', 'Admin', 'resetPassword', 'resetPassword', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('13', '1', '1464080269', '1', '1609129439', '0', '0', '8', '菜单列表', 'admin', 'AdminMenu', 'index', 'adminmenu', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('14', '1', '1461766608', '1', '1608775493', '0', '0', '13', '添加', 'admin', 'AdminMenu', 'add', 'adminmenu/add', '3', '1', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('15', '1', '1461766608', '1', '1608775501', '0', '0', '13', '编辑', 'admin', 'AdminMenu', 'edit', 'adminmenu/edit/{id}', '3', '2', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('16', '1', '1461766608', '1', '1608775506', '0', '0', '13', '删除', 'admin', 'AdminMenu', 'delete', 'adminmenu/delete/{id}', '3', '3', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('17', '1', '1464080269', '1', '1478192928', '0', '0', '2', '管理组', '', '', '', 'articlecate/delete/{id}', '1', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('18', '1', '1464080269', '1', '1620193889', '0', '0', '17', '角色管理', 'admin', 'AdminRole', 'index', 'adminrole', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('19', '1', '1464080269', '1', '1620193887', '0', '0', '18', '添加', 'admin', 'AdminRole', 'add', 'adminrole/add', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('20', '1', '1464080269', '1', '1478192928', '0', '0', '18', '编辑', 'admin', 'AdminRole', 'edit', 'adminrole/edit/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('21', '1', '1464080269', '1', '1478192928', '0', '0', '18', '删除', 'admin', 'AdminRole', 'delete', 'adminrole/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('22', '1', '1462523752', '1', '1484056560', '0', '0', '17', '管理员', 'admin', 'Admin', 'index', 'admin', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('23', '1', '1462525036', '1', '1462525058', '0', '0', '22', '添加', 'admin', 'Admin', 'add', 'admin/add', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('24', '1', '1462525089', '1', '1464932528', '0', '0', '22', '编辑', 'admin', 'Admin', 'edit', 'admin/edit/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('25', '1', '1462525163', '1', '1462525170', '0', '0', '22', '删除', 'admin', 'Admin', 'delete', 'admin/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('26', '1', '1464080269', '1', '1478192928', '0', '0', '2', '日志管理', 'admin', 'AdminLog', 'index', 'adminlog', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('27', '1', '1464080269', '1', '1478192928', '0', '0', '26', '删除', 'admin', 'AdminLog', 'delete', 'adminlog/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('28', '1', '1464080269', '1', '1478192938', '0', '0', '7', '文章分类', 'admin', 'ArticleCate', 'index', 'articlecate', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('29', '1', '1464080269', '1', '1478192928', '0', '0', '28', '添加', 'admin', 'ArticleCate', 'add', 'articlecate/add', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('30', '1', '1464080269', '1', '1478192928', '0', '0', '28', '编辑', 'admin', 'ArticleCate', 'edit', 'articlecate/edit/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('31', '1', '1464080269', '1', '1478192928', '0', '0', '28', '删除', 'admin', 'ArticleCate', 'delete', 'articlecate/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('32', '1', '1464080269', '1', '1478192938', '0', '0', '7', '文章列表', 'admin', 'Article', 'index', 'article', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('33', '1', '1464080269', '1', '1478192928', '0', '0', '32', '添加', 'admin', 'Article', 'add', 'article/add', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('34', '1', '1464080269', '1', '1478192928', '0', '0', '32', '编辑', 'admin', 'Article', 'edit', 'article/edit/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('35', '1', '1464080269', '1', '1478192928', '0', '0', '32', '删除', 'admin', 'Article', 'delete', 'article/delete/{id}', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('36', '1', '1464080269', '1', '1478192928', '0', '0', '32', '导出', 'admin', 'Article', 'export', 'article/export', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('37', '1', '1608775353', '1', '1608775539', '0', '0', '13', '排序', 'admin', 'AdminMenu', 'listOrders', 'adminmenu/listOrders', '3', '10', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('38', '1', '1464080269', '1', '1478192928', '0', '0', '10', '数据库管理', '', '', '', '', '1', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('39', '1', '1464080269', '1', '1478192928', '0', '0', '38', '数据库备份', 'admin', 'Database', 'export', 'database/export', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('40', '1', '1464080269', '1', '1478192928', '0', '0', '39', '备份提交', 'admin', 'Database', 'exportPost', 'database/exportPost', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('41', '1', '1464080269', '1', '1478192928', '0', '0', '38', '数据库还原', 'admin', 'Database', 'import', 'database/import', '2', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('42', '1', '1464080269', '1', '1478192928', '0', '0', '41', '还原提交', 'admin', 'Database', 'importPost', 'database/importPost', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('43', '1', '1464080269', '1', '1478192928', '0', '0', '39', '优化表', 'admin', 'Database', 'optimize', 'database/optimize', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('44', '1', '1464080269', '1', '1478192928', '0', '0', '39', '修复表', 'admin', 'Database', 'repair', 'database/repair', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('45', '1', '1464080269', '1', '1478192928', '0', '0', '38', '删除备份文件', 'admin', 'Database', 'delete', 'database/delete', '3', '0', '_self', '', '0', '1');
INSERT INTO `la_admin_menu` VALUES ('46', '1', '1464080269', '1', '1478192928', '0', '0', '38', '下载', 'admin', 'Database', 'download', 'database/download', '3', '0', '_self', '', '0', '1');
