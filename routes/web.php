<?php

use Illuminate\Support\Facades\Route;

use App\Models\AdminMenu;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ++++++++++++++++++++ 前端 Begin ++++++++++++++++++++ //
$frontendUrl = config('app.http') . 'www.' . config('app.domain');
// 需要二级域名时使用 domain
//Route::domain($frontendUrl)->group(function () {
    Route::namespace('App\\Http\\Controllers\\Home')->group(function () {
        Route::get('/', 'IndexController@index'); // 首页
        Route::get('index.html', 'IndexController@index')->name('index'); // 首页
    });
//});
// ++++++++++++++++++++ 前端 End ++++++++++++++++++++ //


// ++++++++++++++++++++ 后端 Begin ++++++++++++++++++++ //
$backendUrl = config('app.http') . 'manage.' . config('app.domain');
// 后端路由
//Route::domain($backendUrl)->group(function () {
    Route::group(['namespace' => 'App\\Http\\Controllers\\Admin', 'prefix' => 'admin', 'middleware' => ['web']], function () {
        // 登录
        Route::get('login', 'AuthController@login')->name('admin.login');
        Route::any('doLogin', 'AuthController@authenticate')->name('admin.doLogin');
        Route::get('logout', 'AuthController@logout')->name('admin.logout');

        //Route::get('dashboard', [App\Http\Controllers\Admin\IndexController::class, 'dashboard'])->name('admin.dashboard');
        Route::get('delcache', [App\Http\Controllers\Admin\IndexController::class, 'delCache'])->name('admin.delcache'); // 清除缓存
        Route::any('upload', [App\Http\Controllers\Admin\IndexController::class, 'upload'])->name('admin.upload'); // 文件上传
        Route::get('adminrole/getmenus', [App\Http\Controllers\Admin\AdminRoleController::class, 'getMenus'])->name('admin.adminrole.getmenus'); // 角色菜单权限
        Route::get('iframe/icons', [App\Http\Controllers\Admin\IframeController::class, 'icons']);
        Route::any('admin/checkPhone', [App\Http\Controllers\Admin\AdminController::class, 'checkPhone']);
    });

    // 错误页面
    Route::fallback(function () {
        return '页面不存在！';
    });
//});

// 后端路由中间件
Route::group([/*'domain' => $backendUrl,*/ 'middleware' => ['web', 'admin.auth']], function () {
    Route::get('admin', [App\Http\Controllers\Admin\IndexController::class, 'index'])->name('admin.index');

    $menus = AdminMenu::where('status', 1)->get(['app', 'controller', 'action', 'route']);
    foreach ($menus as $menu) {
        if ($menu->controller && $menu->action && $menu->route) {
            $app = ucfirst($menu->app);
            $controller = $menu->controller;
            $action = $menu->action;
            $alias = $menu->app . '/' . strtolower($menu->controller) . '/' . $menu->action;
            $route = $menu->app . '/' . $menu->route;
            if (is_file(app_path('Http/Controllers/' . $app . '/' . $controller . 'Controller.php'))) {
                $className = '\\App\\Http\\Controllers\\' . $app . '\\' . $controller . 'Controller';
                Route::any($route, $className . '@' . $action)->name($alias);
            } else {
                Route::any($route, [App\Http\Controllers\Admin\IndexController::class, 'index'])->name($alias);
            }
            //$routeArr[] = $route;
        }
    }
    //dump($routeArr);die('--');
});
// ++++++++++++++++++++ 后端 End ++++++++++++++++++++ //
