<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\VueAdminMenu;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// 后端API路由
$middlewares = ['init', 'cors'];
//$middlewares = ['cors'];
Route::group(['namespace' => 'App\\Http\\Controllers\\AdminApi', 'middleware' => $middlewares], function () {
    Route::any('auth/login', 'AuthController@login')->name('adminapi.login'); // 登录
    Route::any('auth/logout', 'AuthController@logout')->name('adminapi.logout'); // 退出

    Route::any('common/captcha', 'CommonController@captcha'); // 验证码
    Route::any('common/setting', 'CommonController@setting'); // 设置
    Route::any('common/delCache', 'CommonController@delCache'); // 清除缓存
    Route::any('common/upload', 'CommonController@upload'); // 文件上传
    Route::any('common/download', 'CommonController@download'); // 文件下载
    Route::any('common/getRegion', 'CommonController@getRegion'); // 获取地区

    Route::any('admin/profile', 'AdminController@profile'); // 个人中心
    Route::any('admin/saveProfile', 'AdminController@saveProfile'); // 保存个人信息
    Route::any('admin/menu', 'AdminController@menu'); // 管理员后台菜单
    Route::any('admin/checkLoginName', 'AdminController@checkLoginName'); // 检查登录名是否已存在

    Route::any('dashboard/count', 'DashboardController@count');
    Route::any('dashboard/member', 'DashboardController@member');
    Route::any('dashboard/cms', 'DashboardController@cms');
    Route::any('dashboard/file', 'DashboardController@file');
    Route::any('dashboard/overview', 'DashboardController@overview');
    Route::any('dashboard/chainGrowth', 'DashboardController@chainGrowth');

    Route::any('adminmenu/treeList', 'AdminMenuController@treeList');

    Route::any('article/cateList', 'ArticleController@cateList');
    Route::any('articleCate/handle', 'ArticleCateController@handle');

    Route::any('systemFile/index', 'SystemFileController@index');
    Route::any('systemFile/handle', 'SystemFileController@handle');

    Route::any('region/tree', 'RegionController@tree');

    Route::any('setting/uploadConfig', 'SettingController@uploadConfig');
    Route::any('setting/paymentConfig', 'SettingController@paymentConfig');

    Route::any('database/exportList', 'DatabaseController@exportList');
    Route::any('database/importList', 'DatabaseController@importList');
    Route::any('database/view', 'DatabaseController@view');

    Route::any('message/list', 'MessageController@list');
    Route::any('message/handle', 'MessageController@handle');

    // 测试
    Route::any('test/list', 'TestController@list');
    Route::any('test/info', 'TestController@info');
    Route::any('test/export', 'TestController@export');

    $menus = VueAdminMenu::where('status', 1)->get(['app', 'controller', 'action', 'route']);
    foreach ($menus as $menu) {
        if ($menu->controller && $menu->action && $menu->route) {
            $app = ucfirst($menu->app) . 'Api';
            $controller = $menu->controller;
            $action = $menu->action;
            $alias = $menu->app . 'api/' . strtolower($menu->controller) . '/' . $menu->action;
            $route = $menu->route;
            if ($action == 'index') {
                $action = 'list';
                $route .= '/' . $action;
            } elseif ($action == 'add') {
                $action = 'save';
                $route = str_replace('add', $action, $route);
            } elseif ($action == 'edit') {
                $action = 'info';
                $route = str_replace('edit', $action, $route);
            }
            if (is_file(app_path('Http/Controllers/' . $app . '/' . $controller . 'Controller.php'))) {
                $className = $controller . 'Controller';
                Route::any($route, $className . '@' . $action)->name($alias);
            } else {
                Route::any($route, 'IndexController@index')->name($alias);
            }
            //$routeArr[] = $route;
        }
    }
    //dump($routeArr);die;
});
