<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

// API
Route::namespace('App\\Http\\Controllers\\Api')->group(function () {
    // 文章
    Route::get('articlecate/all', 'ArticleCateController@all'); // 文章分类
    Route::post('article/list', 'ArticleController@list'); // 文章列表
    Route::get('article/detail', 'ArticleController@detail'); // 文章详情
});
