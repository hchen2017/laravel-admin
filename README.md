# Laravel Admin


## 简介
Laravel Admin 是一个用 Laravel 框架开发的后台管理系统。前端包含Layui、Vue2+Element、Vue3+Arco-Design等版本。

[![star](https://gitee.com/hchen2017/laravel-admin/badge/star.svg?theme=dark)](https://gitee.com/hchen2017/laravel-admin/stargazers)
[![fork](https://gitee.com/hchen2017/laravel-admin/badge/fork.svg?theme=dark)](https://gitee.com/hchen2017/laravel-admin/members)
[![License](https://img.shields.io/badge/license-Apache2-yellow)](https://gitee.com/hchen2017/laravel-admin/blob/master/LICENSE)


## 功能
* 用户管理
* 权限管理
* 会员管理
* 内容管理
* 菜单管理
* 文件管理
* 系统管理
* 数据库管理


## 安装
### 前端开发
[adminUI](https://gitee.com/hchen2017/admin-ui)

### 后端安装
- 版本
~~~
laravel 9
~~~
- Windows 安装
~~~
D:\phpstudy\Extensions\php\php8.0.2nts\php.exe C:\ProgramData\ComposerSetup\bin\composer.phar create-project laravel/laravel=9.* laravel-admin
~~~
- 插件
~~~
composer require phpoffice/phpspreadsheet
composer require firebase/php-jwt
composer require qiniu/php-sdk
composer require qcloud/cos-sdk-v5
~~~
- 自动加载自定义的函数
~~~
"autoload": {
    "files": [
        "app/helpers.php"
    ]
},

执行命令 composer dump-autoload
~~~


## 访问
1.配置域名 
~~~
1-1.默认前端 www.domain.test
1-2.默认后端 www.domain.test/admin

当使用子域名时
2-1.前端 www.domain.test
2-2.后端 manage.domain.test/admin
2-2.1.后端 manage.domain.test（优化路由可把admin去掉）

后台账号：admin
后台密码：password
~~~

2.访问地址
~~~
1-1.Layui版本 www.domain.test/admin
1-2.Element版本 www.domain.test/adminapi
1-2.Arco-Design版本 www.domain.test/vue_admin
~~~


## 项目截图

### Layui
- ![article](./public/static/img/layui/article.png)

### Element
- ![index](./public/static/img/element/index.png)
- ![article](./public/static/img/element/article.png)
- ![menu](./public/static/img/element/menu.png)
- ![database](./public/static/img/element/database.png)

### Arco Design
- ![login](./public/static/img/arco-design/login.png)
- ![slide-verify](./public/static/img/arco-design/slide-verify.png)
- ![index](./public/static/img/arco-design/index.png)
- ![role](./public/static/img/arco-design/role.png)
- ![admin](./public/static/img/arco-design/admin.png)
- ![article](./public/static/img/arco-design/article.png)
- ![menu](./public/static/img/arco-design/menu.png)
- ![file](./public/static/img/arco-design/file.png)
- ![region](./public/static/img/arco-design/region.png)
- ![database](./public/static/img/arco-design/database.png)
- ![system](./public/static/img/arco-design/system.png)
- ![test](./public/static/img/arco-design/test.png)


## 交流
交流QQ群：614159657


## 💐 特别鸣谢
- 👉 Laravel：[https://laravel.com](https://laravel.com)
- 👉 Element UI：[https://element.eleme.io](https://element.eleme.io)
- 👉 Arco Design：[https://arco.design/vue/docs/pro/start](https://arco.design/vue/docs/pro/start)


## 捐助
如果这个项目对您有所帮助，您可以点右上角 💘Star💘支持一下，谢谢！！！
<img src="https://gitee.com/hchen2017/admin-ui/raw/master/public/static/img/wxpay.jpg" width="320px;"/>
