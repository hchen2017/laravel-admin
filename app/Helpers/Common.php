<?php

namespace App\Helpers;

/**
 * Class Common Helper
 */
class Common
{
    /**
     * 树状 select
     */
    public static function makeTreeOptions($model, $where = [], $orderBy = [], $defaultOptions = [], $id = 0, $titleField = 'title', $parentField = 'parent_id')
    {
        $options = [];
        if (!empty($defaultOptions)) {
            $options = $defaultOptions;
        }
        $recursive = function ($id, $level) use (&$recursive, $model, $where, $orderBy, &$options, $titleField, $parentField) {
            $space = '';
            for ($i = 0; $i < $level; $i++) {
                $space .= '&nbsp;&nbsp;';
            }
            if ($level) {
                $space .= '&nbsp;├&nbsp;';
            }

            $model = $model::where(array_merge([$parentField => $id], $where));
            if (!empty($orderBy)) {
                foreach ($orderBy as $key => $val) {
                    $model = $model->orderBy($key, $val);
                }
            }

            $results = $model->get();
            foreach ($results as $result) {
                $options[$result->id] = $result->id . '. ' . $space . $result->{$titleField};
                $recursive($result->id, $level + 1);
            }
        };
        $recursive($id, 0);

        return $options;
    }

    /**
     * 树状列表 (包含所有字段)
     */
    public static function getTreeList($model, $where = [], $orderBy = [], $id = 0, $titleField = 'title', $parentField = 'parent_id', $useSpace = false)
    {
        $options = [];
        $recursive = function ($id, $level) use (&$recursive, $model, $where, $orderBy, &$options, $titleField, $parentField, $useSpace) {
            $newModel = $model::where(array_merge([$parentField => $id], $where));
            if (!empty($orderBy)) {
                foreach ($orderBy as $key => $val) {
                    $newModel = $newModel->orderBy($key, $val);
                }
            }

            $results = $newModel->get();
            foreach ($results as $result) {
                if ($useSpace) {
                    $space = '';
                    for ($i = 1; $i < $level; $i++) {
                        //$space .= '&nbsp;&nbsp;';
                        $space .= '&nbsp;│&nbsp;';
                    }
                    if ($level > 1) {
                        $space .= '&nbsp;├─&nbsp;';
                    }
                    $result->$titleField = $space . $result->$titleField;
                }

                if ($model::where(array_merge([$parentField => $result->id], $where))->count()) {
                    $result->has_children = 1;
                }
                $result->level = $level;
                $options[$result->id] = $result;
                $recursive($result->id, $level + 1);
            }
        };

        $recursive($id, 1);

        return $options;
    }

    /**
     * 后台管理员操作记录
     */
    public static function adminLog($request, $logInfo, $adminId = '')
    {
        $data['admin_id'] = $data['created_id'] = $adminId ?: $request->user('admin')->id;
        $data['info'] = $logInfo;
        $data['ip'] = get_IP();
        $data['url'] = $request->route()->getActionName();
        $data['created_at'] = time();

        $model = new \App\Models\AdminLog;
        $model->create($data);
    }

    /**
     * CURL请求
     *
     * @param string $url 请求url地址
     * @param string $method 请求方法 get post
     * @param null $params 参数
     * @param array $headers 请求header信息
     * @return mixed
     */
    public static function curlRequest($url, $method, $params = null, $headers = [])
    {
        $method = strtoupper($method);
        $ci = curl_init();
        curl_setopt($ci, CURLOPT_URL, $url); // 要访问的地址
        switch ($method) {
            case 'POST':
                curl_setopt($ci, CURLOPT_POST, true); // 发送一个常规的Post请求
                if (!empty($params)) {
                    $tmpDataStr = is_array($params) ? http_build_query($params) : $params;
                    curl_setopt($ci, CURLOPT_POSTFIELDS, $tmpDataStr); // Post提交的数据包
                }
                break;
            default:
                curl_setopt($ci, CURLOPT_CUSTOMREQUEST, $method); // 设置请求方式
                break;
        }
        $ssl = preg_match('/^https:\/\//i', $url) ? TRUE : FALSE;
        if ($ssl) {
            curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
            curl_setopt($ci, CURLOPT_SSL_VERIFYHOST, FALSE); // 不从证书中检查SSL加密算法是否存在
        }
        if (!empty($headers)) {
            curl_setopt($ci, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ci, CURLOPT_TIMEOUT, 60); // 设置cURL允许执行的最长秒数
        curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 0); // 在尝试连接时等待的秒数。设置为0，则无限等待。
        curl_setopt($ci, CURLOPT_HEADER, false); // 启用时会将头文件的信息作为数据流输出
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);  // 获取的信息以文件流的形式返回
        curl_setopt($ci, CURLINFO_HEADER_OUT, true); // true 时追踪句柄的请求字符串

        $response = curl_exec($ci);
        //$requestInfo = curl_getinfo($ci);
        $req_errno = curl_errno($ci);
        if ($req_errno) {
            //log_info("---> Curl Request error: " . json_encode($req_errno, JSON_UNESCAPED_UNICODE), 'INFO', 'curl_request');
        }
        curl_close($ci);

        //return $response;
        return json_decode($response, true);
    }
}
