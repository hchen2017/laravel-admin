<?php

// 助手函数

if (!function_exists('is_ssl')) {
    /**
     * 判断是否SSL协议
     * @return boolean
     */
    function is_ssl()
    {
        if (isset($_SERVER['HTTPS']) && ('1' == $_SERVER['HTTPS'] || 'on' == strtolower($_SERVER['HTTPS']))) {
            return true;
        } elseif (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'])) {
            return true;
        }

        return false;
    }
}

if (!function_exists('get_host')) {
    /**
     * 返回带协议的域名
     * @return boolean
     */
    function get_host()
    {
        $host = $_SERVER['HTTP_HOST'];
        $protocol = is_ssl() ? 'https://' : 'http://';

        return $protocol . $host;
    }
}

if (!function_exists('get_IP')) {
    /**
     * 获取客户端IP
     * @return boolean
     */
    function get_IP()
    {
        if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = 'unknown';
        }

        return $ip;
    }
}

/**
 * 格式化字节大小【用于数据库备份】
 *
 * @param number $size 字节数
 * @param string $delimiter 数字和单位分隔符
 * @return string 格式化后的带单位的大小
 */
function format_bytes($size, $delimiter = '')
{
    $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
    for ($i = 0; $size >= 1024 && $i < 5; $i++) {
        $size /= 1024;
    }

    return round($size, 2) . $delimiter . $units[$i];
}

/**
 * 转化数据库保存的文件路径为可以访问的url
 *
 * @param string $filepath
 * @return string
 */
function full_file_url($filepath)
{
    if ($filepath) {
        if (strpos($filepath, 'http') === 0 || strpos($filepath, 'https') === 0) {
            return $filepath;
        } else {
            $fullFileUrl = config('filesystems.disks.uploads.path') . $filepath;
            $fullFileUrl = str_replace('public/', '', $fullFileUrl);
            $fullFileUrl = asset($fullFileUrl);

            return $fullFileUrl;
        }
    } else {
        return asset('static/img/not_image.png');
    }
}

// 生成唯一ID
function get_UUID($prefix = '')
{
    $uuid = md5(uniqid(mt_rand(), true));
    return $prefix . $uuid;
}

/**
 * 生成惟一单号
 * @return string
 */
function create_order_sn()
{
    // ord — 转换字符串第一个字节为 0-255 之间的值
    return date('Ymd') . substr(implode('', array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
}

/**
 * 生成订单号
 */
function make_order_no()
{
    //$yCode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
    //$orderSn = $yCode[intval(date('Y')) - 2019] . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));

    $orderSn = date('Ymd') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));

    return $orderSn;
}

/**
 * 生成随机数字验证码
 * @param int $len
 * @return bool|string
 */
function random_number($len = 6)
{
    $chars = str_repeat('0123456789', 3);
    // 位数过长重复字符串一定次数
    $chars = str_repeat($chars, $len);
    $chars = str_shuffle($chars);
    $str = substr($chars, 0, $len);

    return $str;
}

/**
 * 生成随机字符串生成
 * @param int $len 长度
 * @param string $type 字串类型：0 字母 1 数字 其它 混合
 * @return string
 */
function random_string($len = 6, $type = '')
{
    $str = '';
    switch ($type) {
        case 0:
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            break;
        case 1:
            $chars = str_repeat('0123456789', 3);
            break;
        case 2:
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
        case 3:
            $chars = 'abcdefghijklmnopqrstuvwxyz';
            break;
        case 4:
            $chars = 'ABCDEFGHIJKMNPQRSTUVWXYZ23456789';
            break;
        case 5:
            $chars = 'abcdefghijkmnpqrstuvwxyz23456789';
            break;
        default :
            // 默认去掉了容易混淆的字符oOLl和数字01
            $chars = 'ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789';
            break;
    }
    // 位数过长重复字符串一定次数
    if ($len > 10) {
        $chars = $type == 1 ? str_repeat($chars, $len) : str_repeat($chars, 5);
    }
    $chars = str_shuffle($chars);
    $str = substr($chars, 0, $len);

    /*
    $charsLen = strlen($chars);
    $str = '';
    for ($i = 0; $i < $len; $i++) {
        $str .= $chars[mt_rand(0, $charsLen - 1)];
    }
    */

    return $str;
}

/**
 * 获取文件扩展名
 *
 * @param string $filename
 * @return string
 */
function get_file_extension($filename)
{
    $pathinfo = pathinfo($filename);

    return strtolower($pathinfo['extension']);
}

/**
 * 递归删除文件
 *
 * @param string $dir 文件夹路径
 * @param string/array $fileType 文件类型（.html/.php）
 */
function del_file($dir, $fileType = '')
{
    if (is_dir($dir)) {
        $files = scandir($dir);
        // 打开目录，列出目录中的所有文件并去掉 . 和 ..
        foreach ($files as $filename) {
            if ($filename != '.' && $filename != '..') {
                if (!is_dir($dir . '/' . $filename)) {
                    if (empty($fileType)) {
                        unlink($dir . '/' . $filename);
                    } else {
                        if (is_array($fileType)) {
                            // 正则匹配指定文件
                            if (preg_match($fileType[0], $filename)) {
                                unlink($dir . '/' . $filename);
                            }
                        } else {
                            // 指定包含某些字符串的文件
                            if (false != stristr($filename, $fileType)) {
                                unlink($dir . '/' . $filename);
                            }
                        }
                    }
                } else {
                    del_file($dir . '/' . $filename, $fileType);
                    rmdir($dir . '/' . $filename);
                }
            }
        }
    } else {
        if (file_exists($dir)) unlink($dir);
    }
}

/**
 * 下载文件
 *
 * @param string $filePath 文件路径（public/uploads/csv/2020-01-10）
 * @param string $fileName 文件名（test_20200110.xls）
 */
function download_file($filePath, $fileName)
{
    header('Content-type:text/html;charset=utf-8');
    $file = $filePath . '/' . $fileName;
    // 用以解决中文不能显示出来的问题
    $file = iconv('utf-8', 'gb2312', $file);
    // 首先要判断给定的文件存在与否
    if (!file_exists($file)) {
        exit('没有该文件');
    }
    $fp = fopen($file, 'r');
    $fileSize = filesize($file);
    ob_clean();
    // 下载文件需要用到的头
    header('Content-type: application/octet-stream');
    header('Accept-Ranges: bytes');
    header('Accept-Length:' . $fileSize);
    header('Content-Disposition: attachment; filename=' . $fileName);
    $buffer = 1024;
    $fileCount = 0;
    // 向浏览器返回数据
    while (!feof($fp) && $fileCount < $fileSize) {
        $fileRead = fread($fp, $buffer);
        $fileCount += $buffer;
        echo $fileRead;
    }

    fclose($fp);
}

/**
 * 把返回的数据集转换成Tree
 *
 * @param array $list 要转换的数据集
 * @param string $pk 主键字段
 * @param string $pid parent标记字段
 * @param string $child
 * @param int $root
 * @param bool $isChild
 * @return array
 */
function list_to_tree($list, $pk = 'id', $pid = 'parent_id', $child = 'children', $root = 0, $isChild = false)
{
    // 创建Tree
    $tree = [];
    if (is_array($list)) {
        // 创建基于主键的数组引用
        $refer = [];
        foreach ($list as $key => $item) {
            $refer[$item[$pk]] = &$list[$key];
        }
        foreach ($list as $key => $item) {
            // 判断是否存在parent
            $parentId = $item[$pid];
            if ($root === $parentId) {
                if ($isChild && !isset($list[$key][$child])) {
                    $list[$key][$child] = []; // 子集为空
                }
                $tree[] = &$list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent = &$refer[$parentId];
                    if ($isChild && !isset($list[$key][$child])) {
                        $list[$key][$child] = []; // 子集为空
                    }
                    $parent[$child][] = &$list[$key];
                }
            }
        }
    }

    return $tree;
}

/**
 * 首字母头像
 *
 * @param $text
 * @return string
 */
function letter_avatar($text)
{
    $total = unpack('L', hash('adler32', $text, true))[1];
    $hue = $total % 360;
    list($r, $g, $b) = hsv2rgb($hue / 360, 0.3, 0.9);

    $bg = "rgb({$r},{$g},{$b})";
    $color = '#ffffff';
    $first = mb_strtoupper(mb_substr($text, 0, 1));
    $src = base64_encode('<svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="100" width="100"><rect fill="' . $bg . '" x="0" y="0" width="100" height="100"></rect><text x="50" y="50" font-size="50" text-copy="fast" fill="' . $color . '" text-anchor="middle" text-rights="admin" dominant-baseline="central">' . $first . '</text></svg>');
    $value = 'data:image/svg+xml;base64,' . $src;
    return $value;
}

function hsv2rgb($h, $s, $v)
{
    $r = $g = $b = 0;

    $i = floor($h * 6);
    $f = $h * 6 - $i;
    $p = $v * (1 - $s);
    $q = $v * (1 - $f * $s);
    $t = $v * (1 - (1 - $f) * $s);

    switch ($i % 6) {
        case 0:
            $r = $v;
            $g = $t;
            $b = $p;
            break;
        case 1:
            $r = $q;
            $g = $v;
            $b = $p;
            break;
        case 2:
            $r = $p;
            $g = $v;
            $b = $t;
            break;
        case 3:
            $r = $p;
            $g = $q;
            $b = $v;
            break;
        case 4:
            $r = $t;
            $g = $p;
            $b = $v;
            break;
        case 5:
            $r = $v;
            $g = $p;
            $b = $q;
            break;
    }

    return [
        floor($r * 255),
        floor($g * 255),
        floor($b * 255)
    ];
}


if (!function_exists('api_response')) {
    /**
     * Api 统一返回格式
     * @return boolean
     */
    function api_response($status, $msg = '', $data = [], $error = '', $code = 200)
    {
        $response = [
            'status' => $status,
            'code' => $code,
            'msg' => $msg,
            'data' => $data,
            'error' => $error,
        ];

        return response()->json($response);
    }
}
