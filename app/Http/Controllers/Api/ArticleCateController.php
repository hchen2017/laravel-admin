<?php

namespace App\Http\Controllers\Api;

use App\Services\ArticleCateService;

use Illuminate\Http\Request;

class ArticleCateController extends ApiBaseController
{

    protected $service;

    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->service = new ArticleCateService();
    }

    /**
     * 全部
     */
    public function all(Request $request)
    {
        $params = $request->all();
        $where = [
            ['status', '=', 1]
        ];
        $field = ['id', 'name', 'sub_name'];
        $data = $this->service->getAll($where, $params, $field);

        return api_response(self::STATUS_SUCCESS, 'success', $data);
    }
}
