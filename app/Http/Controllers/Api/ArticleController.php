<?php

namespace App\Http\Controllers\Api;

use App\Services\ArticleService;

use Illuminate\Http\Request;

class ArticleController extends ApiBaseController
{

    protected $service;

    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->service = new ArticleService();
    }

    /**
     * 列表
     */
    public function list(Request $request)
    {
        $params = $request->all();
        $where = [
            ['status', '=', 1]
        ];
        $field = ['id', 'name', 'cover_img', 'source', 'description', 'updated_at'];
        $pageList = $this->service->getLists($where, $params, $field);
        $data = [
            'total' => $pageList['total'],
            'per_page' => $pageList['per_page'],
            'current_page' => $pageList['current_page'],
            'last_page' => $pageList['last_page'],
            'data' => $pageList['data']
        ];

        return api_response(self::STATUS_SUCCESS, 'success', $data);
    }

    /**
     * 详情
     */
    public function detail(Request $request)
    {
        $id = $request->input('id');
        if (!$id) {
            return api_response(self::STATUS_ERROR, '文章ID不能为空', '');
        }

        $where = [
            ['id', '=', $id]
        ];
        $field = ['id', 'name', 'cover_img', 'source', 'description', 'updated_at', 'content', 'count_views'];
        $data = $this->service->findOne($where, ['method' => 'detail'], $field);

        return api_response(self::STATUS_SUCCESS, 'success', $data);
    }
}
