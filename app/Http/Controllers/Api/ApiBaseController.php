<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ApiBaseController extends Controller
{
    const MSG_SUCCESS = '请求成功';
    const MSG_ERROR = '请求失败';
    const NOT_DATA = '暂无数据';
    const NOT_PARAM = '缺少参数';

    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 0;
    const STATUS_NOTICE = 2;
    const CODE_AUTHFAIL = 401;

    protected $userInfo;

    public function __construct(Request $request)
    {
        //
    }
}
