<?php

namespace App\Http\Controllers\AdminApi;

use App\Models\VueAdmin as Admin;
use App\Helpers\Common;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Exception;

class AuthController extends Controller
{
    /**
     * Login
     */
    public function login(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                $username = trim($request->input('username'));
                $password = trim($request->input('password'));
                $remember = $request->input('remember');
                $captcha_code = trim($request->input('captcha_code'));
                $captcha_key = $request->input('captcha_key') ?? '';

                if (!$username) {
                    throw new Exception('登录名不能为空！');
                }
                if (!$password) {
                    throw new Exception('密码不能为空！');
                }

                $adminInfo = Admin::where('username', $username)->first();
                if (!$adminInfo) {
                    throw new Exception('您的用户名不存在！');
                }
                if (!password_verify($password, $adminInfo['password'])) {
                    throw new Exception('密码错误！');
                }
                if ($captcha_code) {
                    $verifyObj = new \App\Services\Verify();
                    if (!$verifyObj->check($captcha_code, $captcha_key)) {
                        throw new Exception('验证码错误！');
                    }
                }

                $adminInfo = Admin::formatItem($adminInfo);

                $code = config('constants.STATUS_SUCCESS');
                $msg = '登录成功';

                $token = md5(time() . uniqid(). mt_rand(1000, 9999));
                $token_expire = time() + 3600 * 6;
                // 记住密码
                if ($remember) {
                    $token_expire = time() + 3600 * 24 * 7;
                }

                $jwt_data = [
                    'admin_id' => $adminInfo['id'],
                    'login_time' => time(),
                    'login_ip' => get_IP(),
                ];
                $jwt_token = \App\Services\AuthToken::create($jwt_data, $token_expire);
                //$adminInfo['auth_token'] = $jwt_token;
                // 记录登录时间及IP等信息
                $data = [
                    'last_login_time' => time(),
                    'last_login_ip' => get_IP(),
                    'token' => $token,
                    'token_expire' => $token_expire
                ];
                $record = Admin::where('id', $adminInfo['id'])->update($data);
                if (!$record) {
                    throw new Exception('服务异常，请重新登录！');
                }
                // TODO 用cache保存，不能实现多人登录同一个账号（退出时会清空缓存），直接用JWT也是可行的
                // 用缓存保存登录信息
                cache(['auth_admin_id-' . $adminInfo['id'] => $adminInfo], $token_expire - time());

                // 日志
                Common::adminLog($request, '管理员登录', $adminInfo['id']);
            } else {
                throw new Exception('请求方式不正确');
            }
        } catch (Exception $e) {
            $code = config('constants.STATUS_ERROR');
            $msg = $e->getMessage();
        }

        return api_response($code, $msg, [
            //'token' => $token ?? ''
            'token' => $jwt_token ?? '',
        ]);
    }

    /**
     * Logout
     */
    public function logout(Request $request)
    {
        $token = $request->input('token');
        $data = [
            'token' => '',
            'token_expire' => 0
        ];
        $result = Admin::where('token', $token)->update($data);
        if ($result === false) {
            return api_response(config('constants.STATUS_ERROR'), 'Error', []);
        }

        // 只能删除特定的缓存标签
        //Cache::flush();

        return api_response(config('constants.STATUS_SUCCESS'), '', []);
    }
}
