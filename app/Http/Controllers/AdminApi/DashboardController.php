<?php

namespace App\Http\Controllers\AdminApi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class DashboardController extends BaseController
{
    public function index(Request $request)
    {
        return api_response(self::STATUS_SUCCESS, '', []);
    }

    /**
     * 数据总计
     */
    public function count(Request $request)
    {
        $where = [
            ['status', '<>', 90],
        ];
        //$firstDay = date('Y-m-01') . ' 00:00:00';
        //$lastDay = date('Y-m-d', strtotime("$firstDay +1 month -1 day")) . ' 23:59:59';
        $firstDay = mktime(0,0,0, date('m'),1, date('Y'));
        $lastDay = mktime(23,59,59, date('m'), date('t'), date('Y'));
        // 总订单
        $totalOrder = mt_rand(800, 4000);
        // 总订单金额
        $orderAmount = mt_rand(800, 4000);
        // 总商品
        $totalGoods = mt_rand(800, 4000);
        // 总文章
        $totalArticle = mt_rand(800, 4000);
        // 本月会员
        $monthMember = mt_rand(800, 4000);
        // 总会员
        $totalMember = mt_rand(800, 4000);

        $data = [
            'month_member' => $monthMember,
            'total_member' => $totalMember,
            'order_amount' => $orderAmount,
            'total_article' => $totalArticle,
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * 会员统计图
     */
    public function member(Request $request)
    {
        $where = [
            ['status', '<>', 90]
        ];
        $statistics = \App\Services\ArticleService::getCharts($where);
        foreach ($statistics['count'] as $count) {
            $statistics['count2'][] = mt_rand(1, 300);
        }
        $data = [
            'dates' => [date('Y-m-d', strtotime('-30 day')), date('Y-m-d')],
            'days' => $statistics['day'],
            'news' => $statistics['count'],
            'actives' => $statistics['count2'],
            'totals' => $statistics['totalViews'],
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * 内容统计图
     */
    public function cms(Request $request)
    {
        $data = [
            'category' => 11,
            'article' => 297,
            'days' => ['友链', '音乐', '下载', '视频', '轮播', '产品', '文章', '新闻', '案例', '游戏'],
            'counts' => [4, 4, 3, 10, 2, 18, 7, 1, 12, 33],
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * 文件统计图
     */
    public function file(Request $request)
    {
        $data = [
            'total' => 63,
            'counts' => [
                [
                    'name' => '图片',
                    'value' => 43
                ],
                [
                    'name' => '图片',
                    'value' => 43
                ],
                [
                    'name' => '图片',
                    'value' => 43
                ],
                [
                    'name' => '视频',
                    'value' => 1
                ],
                [
                    'name' => '音频',
                    'value' => 1
                ],
                [
                    'name' => '文档',
                    'value' => 3
                ],
                [
                    'name' => '其它',
                    'value' => 6
                ],
            ],
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * 数据总览统计图
     */
    public function overview(Request $request)
    {
        $statistics = \App\Services\ArticleService::getCharts();
        foreach ($statistics['day'] as $day) {
            // 会员数
            $statistics['members'][] = mt_rand(200, 800);
            // 文章数
            $statistics['articles'][] = mt_rand(600, 2000);
            // 点击量
            $statistics['clicks'][] = mt_rand(900, 9000);
            // 分享量
            $statistics['shares'][] = mt_rand(800, 4000);
        }
        $data = [
            'members' => $statistics['members'],
            'articles' => $statistics['articles'],
            'clicks' => $statistics['clicks'],
            'shares' => $statistics['shares'],
            'days' => $statistics['day'],
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * 趋势
     */
    public function chainGrowth(Request $request)
    {
        $statistics = \App\Services\ArticleService::getCharts();
        foreach ($statistics['day'] as $day) {
            $statistics['members'][] = mt_rand(1000, 3000);
            $statistics['articles'][] = mt_rand(2000, 5000);
        }

        $data = [
            'count' => mt_rand(1000, 3000),
            'growth' => mt_rand(20, 100) . '.' . mt_rand(11, 99), // 趋势
            'chartData' => [
                'counts' => [$statistics['members'], $statistics['articles']],
                'days' => $statistics['day']
            ]
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }
}
