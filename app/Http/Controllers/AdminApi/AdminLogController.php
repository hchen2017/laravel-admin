<?php

namespace App\Http\Controllers\AdminApi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class AdminLogController extends BaseController
{
    public function initialize(Request $request)
    {
        parent::initialize($request);

        $this->modelName = 'AdminLog';
    }

    /**
     * 筛选条件
     */
    protected function getFilterWhere($request)
    {
        $param = $request->all();
        $where = [];
        if (isset($param['range_date']) && $param['range_date']) {
            $rangeDate = $param['range_date'];
            $startTime = strtotime($rangeDate[0]);
            $endTime = strtotime($rangeDate[1]);
            $where[] = ['created_at', '>=', $startTime];
            $where[] = ['created_at', '<=', $endTime];
        }

        return $where;
    }

    protected function afterInsert($data)
    {
        return ['code' => 1, 'msg' => ''];
    }

    protected function afterUpdate($data, $info = [])
    {
        return ['code' => 1, 'msg' => ''];
    }

    protected function afterDelete($data)
    {
        return ['code' => 1, 'msg' => ''];
    }
}
