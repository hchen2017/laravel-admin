<?php

namespace App\Http\Controllers\AdminApi;

use App\Services\Config as ConfigService;

use Illuminate\Http\Request;

class SettingController extends BaseController
{
    public function initialize(Request $request)
    {
        parent::initialize($request);

        $this->modelName = 'option';
    }

    /**
     * 文件上传设置
     */
    public function uploadConfig(Request $request)
    {
        $key = 'upload_setting';
        if ($request->isMethod('post')) {
            $param = $request->all();
            $value = [
                'storage' => $param['storage'],
                'qiniu' => $param['qiniu'],
                'tencentyun' => $param['tencentyun'] ?? '',
                'aliyun' => $param['aliyun'] ?? '',
            ];
            $mimetype = [
                'image' => [
                    'max_filesize' => $param['image_size'] ?? 0,
                    'extensions' => $param['image_ext'] ?? ''
                ],
                'video' => [
                    'max_filesize' => $param['video_size'] ?? 0,
                    'extensions' => $param['video_ext'] ?? ''
                ],
                'audio' => [
                    'max_filesize' => $param['audio_size'] ?? 0,
                    'extensions' => $param['audio_ext'] ?? ''
                ],
                'file' => [
                    'max_filesize' => $param['file_size'] ?? 0,
                    'extensions' => $param['file_ext'] ?? ''
                ]
            ];
            $value['mimetype'] = $mimetype;
            $result = ConfigService::setOption($key, $key, $value);
            if ($result === false) {
                return api_response(self::STATUS_ERROR, '设置失败！', []);
            }
            // 删除配置缓存
            ConfigService::delCache($key);

            return api_response(self::STATUS_SUCCESS, self::MSG_SUCCESS, $value);
        }

        // 存储方式
        $storages = config('option.storages');
        // 配置
        $config = [];
        $info = ConfigService::getUploadSetting();
        if ($info) {
            $value = $info;
            $config['storage'] = $value['storage'];
            if (isset($value['qiniu'])) {
                $config['qiniu'] = $value['qiniu'];
            }
            if (isset($value['tencentyun'])) {
                $config['tencentyun'] = $value['tencentyun'];
            }
            if (isset($value['aliyun'])) {
                $config['aliyun'] = $value['aliyun'];
            }
            $mimetype = $value['mimetype'];
            $config['image_ext'] = $mimetype['image']['extensions'];
            $config['image_size'] = $mimetype['image']['max_filesize'];
            $config['video_ext'] = $mimetype['video']['extensions'];
            $config['video_size'] = $mimetype['video']['max_filesize'];
            $config['audio_ext'] = $mimetype['audio']['extensions'];
            $config['audio_size'] = $mimetype['audio']['max_filesize'];
            $config['file_ext'] = $mimetype['file']['extensions'];
            $config['file_size'] = $mimetype['file']['max_filesize'];
        }

        $data = [
            'storages' => $storages,
            'config' => $config
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * 支付设置
     */
    public function paymentConfig(Request $request)
    {
        $key = 'payment_setting';
        if ($request->isMethod('post')) {
            $param = $request->all();
            $value = [
                'pay_method' => $param['pay_method'],
                'app_id' => $param['app_id'] ?? '',
                'app_secret' => $param['app_secret'] ?? '',
                'mch_id' => $param['mch_id'] ?? '',
            ];
            $result = ConfigService::setOption($key, $key, $value);
            if ($result === false) {
                return api_response(self::STATUS_ERROR, '设置失败！', []);
            }

            return api_response(self::STATUS_SUCCESS, self::MSG_SUCCESS, $value);
        }

        // 支付方式
        $payments = config('option.payments');
        // 配置
        $info = ConfigService::getOption($key, $key);

        $data = [
            'payments' => $payments,
            'config' => $info
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }
}
