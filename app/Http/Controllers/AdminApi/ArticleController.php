<?php

namespace App\Http\Controllers\AdminApi;

use Illuminate\Http\Request;

class ArticleController extends BaseController
{

    protected $service;

    public function initialize(Request $request)
    {
        parent::initialize($request);

        $this->modelName = 'Article';

        $this->defaultOrder = [
            ['filed' => 'sorting', 'order' => 'ASC'],
            ['filed' => 'created_at', 'order' => 'DESC'],
        ];
    }

    /**
     * 分类
     */
    public function cateList(Request $request)
    {
        $param = $request->all();

        $className = '\\App\\Models\\' . 'ArticleCate';
        $model = new $className;
        $where = [
            ['status', '=', 1]
        ];
        $list = $model->where($where)->orderBy('sorting', 'ASC')->orderBy('created_at', 'DESC')->get()->toArray();
        if ($list) {
            $list = $model->formatDatas($list);
            $list = list_to_tree($list, 'id', 'parent_id', 'children', 0);
        }
        $data = [
            'data' => $list
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * Eval data for post data save
     *
     * @param Request $request
     * @return array
     */
    protected function evalData($request)
    {
        $param = $request->all();

        $data = [
            'id' => $param['id'],
            'cate_id' => $param['cate_id'],
            'name' => $param['name'],
            'cover_img' => $param['cover_img'],
            'source' => $param['source'],
            'keywords' => $param['keywords'],
            'description' => $param['description'],
            'content' => $param['content'],
            'post_time' => $param['post_time'],
            'is_show' => $param['is_show'],
            'is_top' => $param['is_top'],
            'is_recommend' => $param['is_recommend'],
            'is_comment' => $param['is_comment'],
            'sorting' => $param['sorting'],
            'status' => $param['status'],
        ];
        $data['post_time'] = strtotime($data['post_time']);

        return $data;
    }

    /**
     * 处理列表数据
     */
    protected function handleListData($data)
    {
        $className = '\\App\\Models\\' . $this->modelName;
        $model = new $className;

        $data = $model->formatDatas($data, ['source' => 'backend']);

        return $data;
    }

    /**
     * 筛选条件
     */
    protected function getFilterWhere($request)
    {
        $param = $request->all();
        $where = [];
        if (isset($param['name']) && $param['name']) {
            $where[] = ['name', 'like', '%' . trim($param['name']) . '%'];
        }
        if (isset($param['cate_id']) && $param['cate_id']) {
            $where[] = ['cate_id', '=', $param['cate_id']];
        }
        if (isset($param['range_date']) && $param['range_date']) {
            $rangeDate = $param['range_date'];
            $startTime = strtotime($rangeDate[0]);
            $endTime = strtotime($rangeDate[1]);
            $where[] = ['created_at', '>=', $startTime];
            $where[] = ['created_at', '<=', $endTime];
        }

        return $where;
    }
}
