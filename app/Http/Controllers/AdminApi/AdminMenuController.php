<?php

namespace App\Http\Controllers\AdminApi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class AdminMenuController extends BaseController
{
    public function initialize(Request $request)
    {
        parent::initialize($request);

        $this->modelName = 'VueAdminMenu';

        $this->defaultOrder = [
            ['filed' => 'sorting', 'order' => 'ASC'],
        ];
    }

    /**
     * List
     */
    public function list(Request $request)
    {
        $param = $request->all();

        $className = '\\App\\Models\\' . $this->modelName;
        $model = new $className;
        $orders = $this->defaultOrder;
        $where = $this->getFilterWhere($request);
        if ($this->defaultWhere) {
            $where = array_merge($this->defaultWhere, $where);
        }

        $query = $model->query();
        foreach ($where as $value) {
            $query->where($value[0], $value[1], $value[2]);
        }
        foreach ($orders as $order) {
            $query->orderBy($order['filed'], $order['order']);
        }
        $list = $query->get()->toArray();
        if ($list) {
            // 处理列表数据
            $list = $this->handleListData($list);
        }
        $data = [
            'data' => $list,
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * Info
     */
    public function info(Request $request)
    {
        $param = $request->all();
        if (empty($param['id'])) {
            return api_response(self::STATUS_ERROR, self::NOT_PARAM, '');
        }

        $id = $param['id'];
        $className = '\\App\\Models\\' . $this->modelName;
        $model = new $className;
        $info = $model->find($id);
        if (!$info) {
            return api_response(self::STATUS_ERROR, '不存在！', []);
        }
        $info = $model->formatItem($info, ['source' => 'backend']);
        if ($this->sourceUi == self::SOURCE_UI_2) {
            $info['icon'] = $info['icon2'];
        }

        return api_response(self::STATUS_SUCCESS, '', $info);
    }

    /**
     * Tree
     */
    public function treeList(Request $request)
    {
        $param = $request->all();

        $className = '\\App\\Models\\' . $this->modelName;
        $model = new $className;
        $orders = $this->defaultOrder;
        $where = $this->getFilterWhere($request);
        if ($this->defaultWhere) {
            $where = array_merge($this->defaultWhere, $where);
        }

        $query = $model->query();
        foreach ($where as $value) {
            $query->where($value[0], $value[1], $value[2]);
        }
        foreach ($orders as $order) {
            $query->orderBy($order['filed'], $order['order']);
        }
        $list = $query->select('id', 'name', 'parent_id')->get()->toArray();
        if ($list) {
            // 处理列表数据
            $list = list_to_tree($list, 'id', 'parent_id', 'children', 0);
        }
        $data = [
            'data' => $list,
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * 复制
     */
    public function copy(Request $request)
    {
        $param = $request->all();
        if (empty($param['id'])) {
            return api_response(self::STATUS_ERROR, self::NOT_PARAM, '');
        }

        $id = $param['id'];
        $className = '\\App\\Models\\' . $this->modelName;
        $model = new $className;
        $info = $model->find($id);
        if (!$info) {
            return api_response(self::STATUS_ERROR, '不存在！', []);
        }
        $data = [
            'parent_id' => $param['parent_id'] ? $param['parent_id'] : 0,
            'name' => $info['name'],
            'app' => $info['app'],
            'controller' => $info['controller'],
            'action' => $info['action'],
            'route' => $info['route'],
            'is_route' => $info['is_route'],
            'type' => $info['type'],
            'icon' => $info['icon'],
            'sorting' => $info['sorting'],
            'status' => $info['status'],
        ];
        $result = $model->create($data);
        if ($result === false) {
            return api_response(self::STATUS_ERROR, '失败', []);
        }
        $data['id'] = $result->id;
        if (isset($param['is_child']) && $param['is_child']) {
            // 复制子集
            $childrens = $model->where('parent_id', $info['id'])->select();
            foreach ($childrens as $child) {
                $childData = [
                    'parent_id' => $data['id'],
                    'name' => $child['name'],
                    'app' => $child['app'],
                    'controller' => $child['controller'],
                    'action' => $child['action'],
                    'route' => $child['route'],
                    'is_route' => $child['is_route'],
                    'type' => $child['type'],
                    'icon' => $child['icon'],
                    'sorting' => $child['sorting'],
                    'status' => $child['status'],
                ];
                $childRes = $model->create($childData);
                if ($childRes === false) {
                    return api_response(self::STATUS_ERROR, '复制子集失败', []);
                }
            }
        }

        return api_response(self::STATUS_SUCCESS, '', []);
    }

    /**
     * Eval data for post data save
     *
     * @param Request $request
     * @return array
     */
    protected function evalData($request)
    {
        $param = $request->all();

        $data = [
            'id' => $param['id'],
            'parent_id' => $param['parent_id'] ? $param['parent_id'] : 0,
            'name' => $param['name'],
            'app' => $param['app'],
            'controller' => $param['controller'],
            'action' => $param['action'],
            'route' => $param['route'],
            'is_route' => $param['is_route'],
            'type' => $param['type'],
            'icon' => $param['icon'],
            'sorting' => $param['sorting'],
            'status' => $param['status'],
        ];
        if ($this->sourceUi == self::SOURCE_UI_2) {
            $data['icon2'] = $data['icon'];
            unset($data['icon']);
        }

        return $data;
    }

    /**
     * 处理列表数据
     */
    protected function handleListData($data)
    {
        $className = '\\App\\Models\\' . $this->modelName;
        $model = new $className;

        //$data = $model->formatDatas($data);
        foreach ($data as &$item) {
            $item = $model->formatItem($item);
            if ($this->sourceUi == self::SOURCE_UI_2) {
                $item['icon'] = $item['icon2'];
            }
        }
        $data = list_to_tree($data, 'id', 'parent_id', 'children', 0);

        return $data;
    }

    /**
     * 筛选条件
     */
    protected function getFilterWhere($request)
    {
        $param = $request->all();
        $where = [];
        if (isset($param['status']) && strlen($param['status']) > 0) {
            $where[] = ['status', '=', $param['status']];
        }

        return $where;
    }
}
