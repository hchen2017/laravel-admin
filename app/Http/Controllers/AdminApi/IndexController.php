<?php

namespace App\Http\Controllers\AdminApi;

use Illuminate\Http\Request;

class IndexController extends BaseController
{
    public function index(Request $request)
    {
        return api_response(config('constants.STATUS_SUCCESS'), '', []);
    }
}
