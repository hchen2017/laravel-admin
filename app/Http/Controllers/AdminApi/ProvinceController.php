<?php

namespace App\Http\Controllers\AdminApi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class ProvinceController extends BaseController
{
    public function initialize(Request $request)
    {
        parent::initialize($request);

        $this->modelName = 'Region';

        $this->defaultWhere = [
            ['level', '=', 1]
        ];
        $this->defaultOrder = [
            ['filed' => 'id', 'order' => 'ASC'],
        ];
    }
}
