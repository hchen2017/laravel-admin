<?php

namespace App\Http\Controllers\AdminApi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class MemberController extends BaseController
{
    public function initialize(Request $request)
    {
        parent::initialize($request);

        $this->modelName = 'Member';
    }

    /**
     * 统计
     */
    public function chart(Request $request)
    {
        $where = [
            ['status', '<>', 90]
        ];
        $statistics = \App\Services\ArticleService::getCharts();
        foreach ($statistics['count'] as $count) {
            $statistics['count2'][] = mt_rand(1, 300);
        }

        $data = [
            'count' => [
                'yesterday' => 7,
                'today' => 10,
                'week' => 21,
                'month' => 134,
                'year' => 3763,
                'total' => 168853
            ],
            'member' => [
                'dates' => [date('Y-m-d', strtotime('-30 day')), date('Y-m-d')],
                'days' => $statistics['day'],
                'news' => $statistics['count'],
                'actives' => $statistics['count2'],
                'totals' => $statistics['totalViews'],
            ],
            'cms' => [
                'category' => 11,
                'article' => 297,
                'days' => ['友链', '音乐', '下载', '视频', '轮播', '产品', '文章', '新闻', '案例', '游戏'],
                'counts' => [4, 4, 3, 10, 2, 18, 7, 1, 12, 33],
            ],
            'file' => [
                'total' => 63,
                'counts' => [
                    [
                        'name' => '图片',
                        'value' => 43
                    ],
                    [
                        'name' => '图片',
                        'value' => 43
                    ],
                    [
                        'name' => '图片',
                        'value' => 43
                    ],
                    [
                        'name' => '视频',
                        'value' => 1
                    ],
                    [
                        'name' => '音频',
                        'value' => 1
                    ],
                    [
                        'name' => '文档',
                        'value' => 3
                    ],
                    [
                        'name' => '其它',
                        'value' => 6
                    ],
                ],
            ]
        ];

        return api_response(self::STATUS_SUCCESS, '', $data);
    }

    /**
     * 处理列表数据
     */
    protected function handleListData($data)
    {
        $className = '\\App\\Models\\' . $this->modelName;
        $model = new $className;

        $data = $model->formatDatas($data, ['source' => 'backend']);

        return $data;
    }

    /**
     * 筛选条件
     */
    protected function getFilterWhere($request)
    {
        $param = $request->all();
        $where = [];
        if (isset($param['name']) && $param['name']) {
            $where[] = ['name', 'like', '%' . trim($param['name']) . '%'];
        }
        if (isset($param['mobile']) && $param['mobile']) {
            $where[] = ['mobile', 'like', '%' . trim($param['mobile']) . '%'];
        }
        if (isset($param['range_date']) && $param['range_date']) {
            $rangeDate = $param['range_date'];
            $startTime = strtotime($rangeDate[0]);
            $endTime = strtotime($rangeDate[1]);
            $where[] = ['created_at', '>=', $startTime];
            $where[] = ['created_at', '<=', $endTime];
        }

        return $where;
    }
}
