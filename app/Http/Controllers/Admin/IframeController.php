<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class IframeController extends BaseController
{

    /**
     * Icon 图标
     */
    public function icons() {
        $datas = config('icons');

        return view('admin/iframe/icons', [
            'datas' => $datas
        ]);
    }
}
