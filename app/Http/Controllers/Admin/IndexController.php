<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class IndexController extends BaseController
{
    /**
     * Index
     */
    public function index(Request $request)
    {
        $sidebar = $this->initMenu($request);

        return view('admin/layouts/app', [
            'adminInfo' => $this->admin,
            'sidebar' => $sidebar
        ]);
    }

    /**
     * 清除缓存
     */
    public function delCache(Request $request)
    {
        Cache::forget('admin_menu');
        //Cache::tags('admin')->flush();

        return response()->json([
            'code' => 1,
            'msg' => '清除缓存成功!'
        ]);
    }

    /**
     * 文件上传
     */
    public function upload(Request $request)
    {
        $method = $request->input('method');
        $respJson = \App\Services\UploaderService::upload($request);
        $result = json_decode($respJson->getContent(), true);
        if ($method == 'layui') {
            return response()->json([
                'code' => 0, // 0表示成功，其它失败
                'msg' => '上传成功',
                'filepath' => $result['filepath'],
                'data' => [
                    'src' => $result['full_filepath'],
                    'title' => ''
                ]
            ]);
        } elseif ($method == 'tinymce') {
            return response()->json([
                'code' => 1, // 1表示成功，其它失败
                'msg' => '上传成功',
                'location' => $result['full_filepath'],
                'data' => [
                    'src' => $result['full_filepath'],
                    'title' => ''
                ]
            ]);
        }

        return $respJson;
    }
}
