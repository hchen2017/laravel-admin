<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Common;

use Illuminate\Http\Request;

class ArticleCateController extends BaseController
{

    public function initialize(Request $request)
    {
        parent::initialize($request);

        $defaultWhere = $this->getDefaultWhere();
        $defaultWhere[] = ['status', '<>', 90];
        $this->defaultWhere = $defaultWhere;
    }

    /**
     * Delete
     */
    public function delete(Request $request, $id)
    {
        $className = '\\App\\Models\\' . $this->model;
        $model = new $className;

        $result = $model->where('id', $id)->update(['deleted_id' => $this->admin->id, 'deleted_at' =>time(), 'status' => 90]);
        if ($result === false) {
            return response()->json([
                'code' => 0,
                'msg' => '删除失败'
            ]);
        }

        // 写入日志
        $logInfo = $this->admin->name . '删除了一条' . __($this->model) . '数据。';
        Common::adminLog($request, $logInfo);

        return response()->json([
            'code' => 1,
            'msg' => '删除成功!'
        ]);
    }

    /**
     * 筛选条件
     */
    public function getFilterWhere($request)
    {
        $param = $request->all();
        $where = [];
        if (isset($param['name']) && $param['name']) {
            $where[] = ['name', 'like', '%' . trim($param['name']) . '%'];
        }
        if (isset($param['range_date']) && $param['range_date']) {
            $rangeDate = explode(' ~ ', $param['range_date']);
            $where[] = ['created_at', '>=', strtolower($rangeDate[0])];
            $where[] = ['created_at', '<=', strtotime($rangeDate[1])];
        }

        return $where;
    }
}
