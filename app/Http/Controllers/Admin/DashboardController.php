<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class DashboardController extends BaseController
{
    /**
     * 首页
     */
    public function index(Request $request)
    {
        // 当天开始时间
        $todayBegin = strtotime(date('Y-m-d', time()));
        // 当天结束之间
        $todayEnd = $todayBegin + 60*60*24;
        // 本月开始时间
        $monthBegin = mktime(0, 0, 0, date('m'), 1, date('Y'));
        // 本月结束之间
        $monthEnd = mktime(23, 59, 59, date('m'), date('t'), date('Y'));

        // 总文章
        $totalArticle = Article::where('status', '<>', 90)->count();
        // 本月文章
        $monthArticle = Article::where('status', '<>', 90)->where('created_at', '>=', $monthBegin)->where('created_at', '<=', $monthEnd)->count();
        //$monthArticle = Article::where('status', '<>', 90)->whereMonth('created_at', date('m'))->count();
        // 今日文章
        $todayArticle = Article::where('status', '<>', 90)->where('created_at', '>=', $todayBegin)->where('created_at', '<=', $todayEnd)->count();
        //$todayArticle = Article::where('status', '<>', 90)->whereDay('created_at', date('d'))->count();
        // 总会员
        $totalUser = 200000;
        // 本月会员
        $monthUser = mt_rand(1000, 3000);
        // 今日会员
        $todayUser = mt_rand(100, 500);

        $data = [
            'totalArticle' => $totalArticle,
            'monthArticle' => $monthArticle,
            'todayArticle' => $todayArticle,
            'totalUser' => $totalUser,
            'monthUser' => $monthUser,
            'todayUser' => $todayUser,
        ];

        return view('admin/dashboard/index', [
            'data' => $data
        ]);
    }
}
