<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Common;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminLogController extends BaseController
{

    public function initialize(Request $request)
    {
        parent::initialize($request);

        $defaultWhere = $this->getDefaultWhere();
        $defaultWhere[] = ['a.status', '<>', 90];
        $this->defaultWhere = $defaultWhere;
    }

    /**
     * List
     */
    public function index(Request $request)
    {
        $params = $request->all();

        if ($request->ajax()) {
            $className = '\\App\\Models\\' . $this->model;
            $model = new $className;

            // 每页起始条数
            $start = ($params['page'] - 1) * $params['limit'];
            // 每页显示条数
            $length = $params['limit'];
            // 排序条件
            $order_by = 'created_at';
            $order_sort = 'desc';
            $where = $this->getFilterWhere($request);
            if ($this->defaultWhere) {
                $where = array_merge($this->defaultWhere, $where);
            }

            $query = DB::table('admin_logs as a')
                ->join('admins as b', 'a.admin_id', '=', 'b.id');
            $fields = ['a.id', 'a.info', 'a.ip', 'a.url', 'a.created_at', 'b.name'];
            $count = $query->count();
            $data = $query
                ->where($where)
                ->select($fields)
                ->offset($start)
                ->limit($length)
                ->orderBy($order_by, $order_sort)
                ->get()
                ->toArray();

            if ($data) {
                // 处理列表数据
                foreach ($data as $item) {
                    $item->username = '账号: ' . $item->name;
                }
            }

            $result = [
                'code' => '0',
                'msg' => '',
                'data' => $data,
                'count' => $count,
            ];

            return response()->json($result);
        }

        return view('admin/' . strtolower($this->model) . '/' . $this->indexView, [
            'params' => $params
        ]);
    }

    /**
     * 筛选条件
     */
    public function getFilterWhere($request)
    {
        $param = $request->all();
        $where = [];
        if (isset($param['name']) && $param['name']) {
            $where[] = ['name', 'like', '%' . trim($param['name']) . '%'];
        }
        if (isset($param['range_date']) && $param['range_date']) {
            $rangeDate = explode(' ~ ', $param['range_date']);
            $where[] = ['created_at', '>=', strtolower($rangeDate[0])];
            $where[] = ['created_at', '<=', strtotime($rangeDate[1])];
        }

        return $where;
    }
}
