<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Builder;

class IndexController extends BaseController
{
    /**
     * Index
     */
    public function index(Request $request)
    {
        //$this->test($request);

        return 'welcome';
    }

    public function test(Request $request)
    {
        DB::connection()->enableQueryLog();
        $model = new \App\Models\ArticleCate();
        //$dataWithItems = $model->get();
//        $dataWithItems = $model->whereHas('articles', function (Builder $query) {
//            $query->where('id', '>', 0);
//        })->get();
        //$dataWithItems = $model->with('articles')->get();
        $dataWithItems = $model->with(['articles' => function ($query) {
            $query->where('id', '>', 0); // 关联表的条件
        }])->get();
        foreach ($dataWithItems as $item) {
            dump($item->toArray());
            //$cc = $item->toArray();
            //$cc['items'] = $item->articles->toArray();
            if ($item['id'] == 1) {
                dump($item->articles->toArray());
            }
        }
        dump($dataWithItems);
        dump(DB::getQueryLog());
    }
}
