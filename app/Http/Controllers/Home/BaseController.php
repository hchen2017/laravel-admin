<?php

namespace App\Http\Controllers\Home;

use Illuminate\Routing\Controller as Base;

class BaseController extends Base
{

    /**
     * 构造函数
     */
    public function __construct()
    {
        //
    }
}
