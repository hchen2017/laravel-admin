<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{

    /**
     * 跨越
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('Access-Control-Allow-Origin', '*');
        $response->header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        //$response->header('Access-Control-Allow-Methods',  'POST, GET, OPTIONS, PUT, DELETE');
        $response->header('Access-Control-Allow-Credentials', 'true');
        $response->header('Access-Control-Max-Age', '10000');
        //$response->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin');
        //$response->header('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
        $response->header('Access-Control-Allow-Headers', '*'); // 支持头部带token字段

        return $response;
    }
}
