<?php

namespace App\Http\Middleware;

use Closure;

class Initialize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (method_exists($request->route()->getController(), 'initialize')) {
            $request->route()->getController()->initialize($request);
        }

        return $next($request);
    }
}
