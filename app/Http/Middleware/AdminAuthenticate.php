<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AdminAuthenticate extends Middleware
{
    /**
     * Handle
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $guard = 'admin';
        if (Auth::guard($guard)->guest()) {
            return redirect(route('admin.login'));
        }

        if ($request->route()->getName() == null) {
            return redirect(route('admin.index'));
        } else {
            if (method_exists($request->route()->getController(), 'initialize')) {
                $request->route()->getController()->initialize($request);
            }
        }

        return $next($request);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
//    protected function redirectTo($request)
//    {
//        if (! $request->expectsJson()) {
//            return route('admin.login');
//        }
//    }
}
