<?php

namespace App\Exceptions;

use Exception;

class BaseException extends Exception
{
    // HTTP 状态码 404,200
    public $code = 400;

    public $status = 0;

    // 错误具体信息
    public $msg = '参数错误';

    public function __construct($params = [])
    {
        if (!is_array($params)) {
            return;
        }

        if (array_key_exists('code', $params)) {
            $this->code = $params['code'];
        }
        if (array_key_exists('status', $params)) {
            $this->status = $params['status'];
        }
        if (array_key_exists('msg', $params)) {
            $this->msg = $params['msg'];
        }
    }
}
