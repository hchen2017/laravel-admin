<?php

namespace App\Services;

use App\Models\ArticleCate;
use App\Models\Article;

use Exception;

class ArticleCateService
{
    /**
     * 全部
     */
    public function getAll($where = [], $params = [], $field = '*')
    {
        $model = new ArticleCate();
        $query = $model->query();
        foreach ($where as $value) {
            $query->where($value[0], $value[1], $value[2]);
        }

        $data = $query->orderBy('created_at', 'desc')->limit(3)->select($field)->get();

        return $data;
    }

    /**
     * 最新分类文章
     */
    public function getLatestArticles($where = [], $params = [], $field = '*')
    {
        $model = new ArticleCate();
        $query = $model->query();
        foreach ($where as $value) {
            $query->where($value[0], $value[1], $value[2]);
        }

        $data = $query->orderBy('created_at', 'desc')->limit(3)->select($field)->get()->toArray();
        foreach ($data as &$item) {
            $articles = Article::where('cate_id', $item['id'])->where('status', 1)->orderBy('updated_at', 'desc')->take(5)->select(['id', 'name'])->get()->toArray();
            $item['articles'] = $articles;
        }

        return $data;
    }
}
