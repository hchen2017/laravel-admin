<?php

namespace App\Models;

use App\Services\FileService;

class File extends Base
{

    protected $table = 'files';

    protected $guarded = [];


    /**
     * 格式化列表数据
     */
    public static function formatDatas($list, $params = '')
    {
        foreach ($list as &$item) {
            $item = self::formatItem($item, $params);
        }

        return $list;
    }

    /**
     * 格式化单个数据
     */
    public static function formatItem($item, $params = '')
    {
        if (!$item) {
            return '';
        }

        $item['full_file_url'] = full_file_url($item['file_url']);
        $item['file_size'] = FileService::sizeFormat($item['file_size']);

        return $item;
    }

    /**
     * 删除文件
     */
    public static function delFile($data)
    {
        // 删除文件
        $storage = $data['storage'];
        $file_url = $data['file_url'];
        if ($storage == 'local') {
            // 本地服务器
            $file_url = substr($file_url, stripos($file_url, config('path.upload_disk')));
            del_file($file_url);
            return ['code' => 1, 'msg' => ''];
        } elseif ($storage == 'qiniu') {
            // 七牛
            $qiniu = new \App\Services\Qiniu();
            $key = substr($file_url, stripos($file_url, $qiniu->url));
            $result = $qiniu->delFile($key);
            return ['code' => 1, 'msg' => $result];
        } elseif ($storage == 'tencentyun') {
            // 腾讯云
            $qcloud = new \App\Services\Qcloud();
            $key = substr($file_url, stripos($file_url, $qcloud->url));
            $result = $qcloud->delFile($key);
            return ['code' => 1, 'msg' => $result];
        }
    }
}
