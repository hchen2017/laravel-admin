<?php

namespace App\Models;

class ArticleCate extends Base
{

    protected $table = 'article_cate';

    protected $guarded = [];


    public function articles()
    {
        return $this->hasMany(Article::class, 'cate_id');
    }
}
