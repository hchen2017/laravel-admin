<?php

namespace App\Models;

class Article extends Base
{

    protected $table = 'articles';

    protected $guarded = [];

    public static $tabeName = 'articles';

    public static function _after_insert($data)
    {
        // 封面图
        if (isset($data['cover_img']) && !empty($data['cover_img'])) {
            File::where('file_url', $data['cover_img'])->update(['related_id' => $data['id'], 'related_table' => self::$tabeName, 'tag' => 'single', 'status' => 1]);
        }
        // 图片集
        if (isset($data['images']) && !empty($data['images'])) {
            $images = $data['images'];
            if (!is_array($images)) {
                $images = explode(',', $images);
            }
            foreach ($images as $key => $image) {
                File::where('file_url', $image)->update(['related_id' => $data['id'], 'related_table' => self::$tabeName, 'tag' => 'images', 'sorting' => $key, 'status' => 1]);
            }
        }
        // 附件
        if (isset($data['files']) && !empty($data['files'])) {
            $files = $data['files'];
            if (!is_array($files)) {
                $files = explode(',', $files);
            }
            foreach ($files as $key => $file) {
                File::where('file_url', $file)->update(['related_id' => $data['id'], 'related_table' => self::$tabeName, 'tag' => 'files', 'sorting' => $key, 'status' => 1]);
            }
        }
    }

    public static function _after_update($data)
    {
        // 封面图
        if (isset($data['cover_img']) && !empty($data['cover_img'])) {
            // 处理删除掉的文件
            File::where(['related_id' => $data['id'], 'related_table' => self::$tabeName, 'tag' => 'single'])->where('file_url', '<>', $data['cover_img'])->update(['status' => 0]);

            File::where('file_url', $data['cover_img'])->update(['related_id' => $data['id'], 'related_table' => self::$tabeName, 'tag' => 'single', 'status' => 1]);
        }
        // 图片集
        if (isset($data['images']) && !empty($data['images'])) {
            // 处理删除掉的文件
            File::where(['related_id' => $data['id'], 'related_table' => self::$tabeName, 'tag' => 'images'])->whereNotIn('file_url', $data['images'])->update(['status' => 0]);

            $images = $data['images'];
            if (!is_array($images)) {
                $images = explode(',', $images);
            }
            foreach ($images as $key => $image) {
                File::where('file_url', $image)->update(['related_id' => $data['id'], 'related_table' => self::$tabeName, 'tag' => 'images', 'sorting' => $key, 'status' => 1]);
            }
        }
        // 附件
        if (isset($data['files']) && !empty($data['files'])) {
            // 处理删除掉的文件
            File::where(['related_id' => $data['id'], 'related_table' => self::$tabeName, 'tag' => 'files'])->whereNotIn('file_url', $data['files'])->update(['status' => 0]);

            $files = $data['files'];
            if (!is_array($files)) {
                $files = explode(',', $files);
            }
            foreach ($files as $key => $file) {
                File::where('file_url', $file)->update(['related_id' => $data['id'], 'related_table' => self::$tabeName, 'tag' => 'files', 'sorting' => $key, 'status' => 1]);
            }
        }
    }

    public static function _after_delete($data)
    {
        if (is_numeric($data) || is_string($data)) {
            $data = self::find($data);
        }

        // 封面图
        if (isset($data['cover_img']) && !empty($data['cover_img'])) {
            File::where('file_url', $data['cover_img'])->update(['status' => 0]);
        }
        // 图片集
        if (isset($data['images']) && !empty($data['images'])) {
            $images = $data['images'];
            File::whereIn('file_url', $images)->update(['status' => 0]);
        }
        // 附件
        if (isset($data['files']) && !empty($data['files'])) {
            $files = $data['files'];
            File::whereIn('file_url', $files)->update(['status' => 0]);
        }
    }

    /**
     * 格式化列表数据
     */
    public static function formatDatas($list, $params = '')
    {
        foreach ($list as &$item) {
            $item = self::formatItem($item, $params);
        }

        return $list;
    }

    /**
     * 格式化单个数据
     */
    public static function formatItem($item, $params = '')
    {
        if (!$item) {
            return '';
        }

        $full_cover_img = '';
        if ($item['cover_img']) {
            $full_cover_img = full_file_url($item['cover_img']);
        }
        $item['full_cover_img'] = $full_cover_img;
        if ($item['post_time']) {
            $item['post_time'] = date('Y-m-d H:i:s', $item['post_time']);
        }

        if (isset($params['source']) && $params['source'] == 'backend') {
            // 状态
            switch ($item['status']) {
                case 1:
                    $item['status_name'] = '显示';
                    break;
                case 2:
                    $item['status_name'] = '隐藏';
                    break;
                case 90:
                    $item['status_name'] = '删除';
                    break;
                default: break;
            }

            // 分类
            $cate_name = ArticleCate::where('id', $item['cate_id'])->pluck('name')->first();
            $item['cate_name'] = $cate_name;
        } else {
            // 时间
            $updated_at = $item['updated_at'];
            $item['updated_at'] && $item['updated_at'] = substr($updated_at, 0, 10);

            if (isset($params['method']) && $params['method'] == 'detail') {
                // 上一篇
                $prev_page = Article::where('updated_at', '<', strtotime($updated_at))->select('id', 'name')->first();
                $item['prev_page'] = $prev_page ? $prev_page->toArray() : '';
                // 下一篇
                $next_page = Article::where('updated_at', '>', strtotime($updated_at))->select('id', 'name')->first();
                $item['next_page'] = $next_page ? $next_page->toArray() : '';
            }
        }

        return $item;
    }
}
