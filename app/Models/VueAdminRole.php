<?php

namespace App\Models;

class VueAdminRole extends Base
{

    protected $table = 'vue_admin_roles';

    protected $guarded = [];


    /**
     * 格式化列表数据
     */
    public static function formatDatas($list, $params = '')
    {
        foreach ($list as &$item) {
            $item = self::formatItem($item, $params);
        }

        return $list;
    }

    /**
     * 格式化单个数据
     */
    public static function formatItem($item, $params = '')
    {
        if (!$item) {
            return '';
        }

        //$item['access_ids'] = explode(',', $item['access']);
        $accessIds = explode(',', $item['access']);
        foreach ($accessIds as &$accessId) {
            $accessId = (int) $accessId;
        }
        $item['access_ids'] = $accessIds;

        return $item;
    }

    /**
     * 是否被使用过
     */
    public static function isUsed($id)
    {
        if (!$id) {
            return ['code' => 0, 'msg' => '参数错误'];
        }
        // 是否有管理员
        $roleUser = VueAdminRoleUser::where('role_id', $id)->select('id')->first();
        if ($roleUser) {
            return ['code' => 0, 'msg' => '该角色下有管理员，请先删除管理员'];
        }

        return ['code' => 1, 'msg' => ''];
    }
}
