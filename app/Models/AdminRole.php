<?php

namespace App\Models;

class AdminRole extends Base
{

    protected $table = 'admin_roles';

    protected $guarded = [];
}
