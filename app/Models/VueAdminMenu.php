<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\SoftDeletes;

class VueAdminMenu extends Base
{
    //use SoftDeletes;

    protected $table = 'vue_admin_menu';

//    protected $fillable = [
//        'id', 'created_id', 'created_at', 'updated_id', 'updated_at', 'deleted_id', 'deleted_at', 'parent_id', 'name', 'icon', 'sorting', 'status'
//    ];

    protected $guarded = []; // 不可以注入数据字段


    /**
     * 格式化列表数据
     */
    public static function formatDatas($list, $params = '')
    {
        foreach ($list as &$item) {
            $item = self::formatItem($item, $params);
        }

        return $list;
    }

    /**
     * 格式化单个数据
     */
    public static function formatItem($item, $params = '')
    {
        if (!$item) {
            return '';
        }

        if ($item['icon']) {
            $item['full_icon'] = full_file_url($item['icon']);
        }

        return $item;
    }

    /**
     * 转为前端路由所需要的树形结构
     */
    public static function toRouteTree($list)
    {
        $tree = list_to_tree($list, 'id', 'parent_id', 'children', 0, true);

        return $tree;
    }

    /**
     * 转为前端路由
     */
    public static function treeToRoutes($treeMenus)
    {
        $subMenu = function ($menus, $level = 0) use (&$subMenu) {
            $outputs = [];
            foreach ($menus as $menu) {
                if ($menu['controller'] == 'Dashboard' || $menu['action'] == 'dashboard') {
                    continue;
                }
                if ($menu['route']) {
                    $path = '/' . $menu['app'] . '/' . $menu['route'];
                    $name = ucfirst($menu['app']);
                    $menu_routes = explode('/', $menu['route']);
                    foreach ($menu_routes as $menu_route) {
                        $name .= ucfirst($menu_route);
                    }
                } else {
                    $path = '/' . $menu['app'] . '/' . strtolower($menu['controller']);
                    $name = ucfirst($menu['app']) . ucfirst($menu['controller']);
                }
                if (strtolower($menu['action']) == 'index') {
                    $component = strtolower($menu['controller']);
                } else {
                    $component = strtolower($menu['controller']) . '_' . strtolower($menu['action']);
                }
                $route = $menu['app'] . '/' . strtolower($menu['controller']) . '/' . $menu['action'];
                if ($level == 0) {
                    $output = [
                        'path' => $path,
                        'component' => 'layout',
                        'redirect' => 'noRedirect',
                        //'alwaysShow' => true,
                    ];
                    if ($menu['children']) {
                        $output['meta'] = [
                            'title' => $menu['name'],
                            'icon' => $menu['icon'],
                            'routes' => [$route]
                        ];
                        $output['alwaysShow'] = true;
                        $output['children'] = $subMenu($menu['children'], $level + 1);
                    } else {
                        $output['children'][] = [
                            'path' => $path,
                            'name' => $name,
                            'component' => $component,
                            'meta' => [
                                'title' => $menu['name'],
                                'icon' => $menu['icon'],
                                'routes' => [$route]
                            ],
                        ];
                    }
                } else {
                    $output = [
                        'path' => $path,
                        'name' => $name,
                        'component' => $component,
                        'meta' => [
                            'title' => $menu['name'],
                            'icon' => $menu['icon'],
                            'routes' => [$route]
                        ],
                    ];
                    if ($menu['type'] == 3) {
                        $output['hidden'] = true;
                        $output['meta']['noCache'] = true;
                    }
                    if ($menu['children']) {
                        $count_child = 0;
                        foreach ($menu['children'] as $child) {
                            if ($child['type'] == 3) {
                                if ($child['route']) {
                                    $path_2 = '/' . $child['app'] . '/' . $child['route'];
                                    $name_2 = ucfirst($child['app']);
                                    $menu_route2s = explode('/', $child['route']);
                                    foreach ($menu_route2s as $menu_route2) {
                                        $name_2 .= ucfirst($menu_route2);
                                    }
                                } else {
                                    $path_2 = '/' . $child['app'] . '/' . strtolower($child['controller']);
                                    $name_2 = ucfirst($child['app']) . ucfirst($child['controller']);
                                }
                                if (strtolower($child['action']) == 'index') {
                                    $component_2 = strtolower($child['controller']);
                                } else {
                                    $component_2 = strtolower($child['controller']) . '_' . strtolower($child['action']);
                                }
                                $route_2 = $child['app'] . '/' . strtolower($child['controller']) . '/' . $child['action'];
                                $output_2 = [
                                    'path' => $path_2,
                                    'name' => $name_2,
                                    'component' => $component_2,
                                    'meta' => [
                                        'title' => $child['name'],
                                        'icon' => $child['icon'],
                                        'routes' => [$route_2]
                                    ],
                                ];
                                $output_2['hidden'] = true;
                                $output_2['meta']['noCache'] = true;
                                $output_2['meta']['activeMenu'] = '/' . $child['app'] . '/' . strtolower($child['controller']);
                                if ($child['action'] == 'edit') {
                                    $output_2['path'] .= '/:id(\d+)';
                                }
                                $outputs[] = $output_2;
                            } else {
                                $output['children'] = $subMenu($menu['children'], $level + 1);
                                $count_child ++;
                            }
                        }
                        if ($count_child > 0) {
                            $output['component'] = 'nested';
                            $output['redirect'] = 'noRedirect';
                            //unset($output['name']);
                        }
                    }
                }
                $outputs[] = $output;
            }
            return $outputs;
        };

        return $subMenu($treeMenus);
    }

    /**
     * 转为前端路由 -- Arco Design
     */
    public static function treeToRoutesArco($treeMenus)
    {
        $subMenu = function ($menus, $level = 0) use (&$subMenu) {
            $outputs = [];
            foreach ($menus as $menu) {
                $hideInMenu = false;
                if ($menu['type'] == 3) {
                    //continue;
                    $hideInMenu = true;
                }
                if ($menu['controller'] == 'Dashboard' || $menu['action'] == 'dashboard') {
                    //continue;
                }
                if ($menu['route']) {
                    $path = '/' . $menu['route'];
                    $name = '';
                    $menu_routes = explode('/', $menu['route']);
                    foreach ($menu_routes as $menu_route) {
                        $name .= ucfirst($menu_route);
                    }
                } else {
                    $path = '/' . strtolower($menu['controller']);
                    $name = ucfirst($menu['controller']);
                }
                $component = strtolower(preg_replace('/(?<=[a-z])([A-Z])/', '-$1', $menu['route']));
                if (strtolower($menu['action']) == 'index') {
                    $component .= '/index';
                }
                $locale = lcfirst($menu['controller']);
                if  ($menu['action'] != 'index') {
                    $locale .= ucfirst($menu['action']);
                }
                $output = [
                    'path' => $path,
                    'name' => $name,
                    'component' => $component,
                    'meta' => [
                        'locale' => $locale,
                        'title' => $menu['name'],
                        'requiresAuth' => true,
                        'hideInMenu' => $hideInMenu,
                        'order' => $menu['sorting'],
                    ],
                ];
                if ($menu['icon']) {
                    $output['meta']['icon'] = $menu['icon2'];
                }
                if ($menu['children']) {
                    $output['children'] = $subMenu($menu['children'], $level + 1);
                }
                if (isset($output['children']) && empty($output['children'])) {
                    unset($output['children']);
                }
                $outputs[] = $output;
            }
            return $outputs;
        };

        return $subMenu($treeMenus);
    }
}
