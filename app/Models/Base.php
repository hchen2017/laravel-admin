<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U'; // 时间存时间戳

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * Query执行后的操作
     * @param $data
     */
    public function afterQuery($data)
    {
        //
    }

    public static function _after_insert($data)
    {
        //
    }

    public static function _after_update($data)
    {
        //
    }

    public static function _after_delete($data)
    {
        //
    }

    /**
     * @param $date
     * @return bool|string
     */
//    public function getCreatedAtAttribute($date)
//    {
//        if (!$date) {
//            return '';
//        }
//        return date('Y-m-d H:i:s', $date);
//    }

    /**
     * @param $date
     * @return bool|string
     */
//    public function getUpdatedAtAttribute($date)
//    {
//        if (!$date) {
//            return '';
//        }
//        return date('Y-m-d H:i:s', $date);
//    }

    /**
     * 格式化列表数据
     */
    public static function formatDatas($list, $params = '')
    {
        foreach ($list as &$item) {
            $item = self::formatItem($item, $params);
        }

        return $list;
    }

    /**
     * 格式化单个数据
     */
    public static function formatItem($item, $params = '')
    {
        return $item;
    }

    /**
     * 删除
     */
    public static function doDelete($id)
    {
        return self::where('id', $id)->delete();
    }
}
