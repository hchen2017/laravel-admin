<?php

namespace App\Models;

class VueAdminRoleUser extends Base
{

    public $timestamps = false;

    protected $table = 'vue_admin_role_user';

    protected $guarded = [];


    public function adminRole()
    {
        return $this->belongsTo(VueAdminRole::class,'role_id', 'id');
    }
}
