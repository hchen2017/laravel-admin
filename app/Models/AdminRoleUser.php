<?php

namespace App\Models;

class AdminRoleUser extends Base
{

    public $timestamps = false;

    protected $table = 'admin_role_user';

    protected $guarded = [];
}
