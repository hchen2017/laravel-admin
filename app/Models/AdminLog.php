<?php

namespace App\Models;

class AdminLog extends Base
{

    protected $table = 'admin_logs';

    protected $guarded = [];
}
