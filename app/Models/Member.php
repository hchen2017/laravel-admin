<?php

namespace App\Models;

class Member extends Base
{

    protected $table = 'users';

    protected $guarded = [];

    /**
     * 格式化列表数据
     */
    public static function formatDatas($list, $params = '')
    {

        foreach ($list as &$item) {
            $item = self::formatItem($item, $params);
        }

        return $list;
    }

    /**
     * 格式化单个数据
     */
    public static function formatItem($item, $params = '')
    {
        if (!$item) {
            return '';
        }

        if ($item['avatar']) {
            $item['full_avatar'] = full_file_url($item['avatar']);
        } else {
            $item['full_avatar'] = letter_avatar($item['name']);
        }

        return $item;
    }
}
