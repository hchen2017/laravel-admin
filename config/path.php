<?php

return [
    // 文件上传路径
    'upload_disk' => 'uploads', // 上传文件夹
    'upload_path' => 'uploads/', // 数据库存储地址
];
