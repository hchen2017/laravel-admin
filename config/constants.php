<?php

// 常量
return [
//    'MSG_SUCCESS' => '请求成功',
//    'MSG_ERROR' => '请求失败',
//    'NOT_DATA' => '暂无数据',
//    'NOT_PARAM' => '缺少参数',

    'STATUS_SUCCESS' => 1,
    'STATUS_ERROR' => 0,
    'STATUS_NOTICE' => 2,
    'CODE_AUTHFAIL' => 401,
];
