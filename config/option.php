<?php

return [
    // 邮件
    'mail' => [
        'driver' => 'smtp',
        'host' => 'smtp.163.com',
        'port' => 25,
        'username' => '',
        'password' => '', // pop3授权密码
        'from' => '', // 163邮箱服务器要求from和username必须一致
        'from_name' => '',
        'encryption' => 'tls',

        '163' => [
            'host' => 'smtp.163.com',
            'port' => 25,
            'username' => '',
            'password' => '',
            'from' => '',
        ],
        // 163使用465端口也可以
        /*'163' => [
            'host' => 'smtp.163.com',
            'port' => 465,
            'username' => ',
            'password' => '',
            'from' => '',
            'encryption' => 'ssl',
        ],*/
        'sina' => [
            'host' => 'smtp.sina.com',
            'port' => 25,
            'username' => '',
            'password' => '',
            'from' => '',
        ],
        'qq' => [
            'host' => 'smtp.qq.com',
            'port' => 465,
            'username' => '',
            'password' => '',
            'from' => '',
            'from_name' => '', // 没有生效
            'encryption' => 'ssl',
        ],
        'gmail' => [
            'host' => 'smtp.gmail.com',
            'port' => 465,
            'username' => '',
            'password' => '',
            'from' => '',
            'encryption' => 'ssl',
        ],
        'outlook' => [
            'host' => 'smtp.office365.com',
            'port' => 587,
            'username' => '',
            'password' => '', // 登录密码
            'from' => '',
            'encryption' => 'tls',
        ],
    ],

    // 订单配置
    'order' => [
        // 支付后多长时间内可以退款(天)
        'order_refund' => 7,
        // 下单后多长时间没支付订单失效(秒)，默认30分钟(1800)
        'invalid_time' => 1800, // 定时任务每分钟才执行，会有一点点偏差(小程序那边加时间)
        // 订单多长时间自动签收(秒)，默认3天(10800)
        'received_time' => 3600,
        // 订单签收后多长时间自动关闭(秒)，默认3天(259200)
        'received_invalid_time' => 3600,
        // 订单退款后多长时间未操作(秒)，则视为拒绝，默认2天(172800)，需小于签收后自动关闭时间
        'refund_invalid_time' => 1800,

        // 拼团后多长时间没支付拼团失效(秒)，默认5分钟(300)，即，其他人可继续参团
        'group_invalid_time' => 300,
        // 成团时长（秒数）
        'group_time' => 3600,

        // 订单出货时间(24小时制)，默认每天下午4点
        'outstock_time' => '16:00',
        // 退货订单入库时间(24小时制)，默认每天下午4点
        //'instock_time' => 16,

        // 收益到账时间(秒)，默认订单签收后7天(604800)
        'profit_account_time' => 604800,
    ],

    // 物流 https://market.aliyun.com/products/57124001/cmapi034466.html?spm=5176.2020520132.101.3.33967218Lu4Jps
    'express' => [
        'app_key' => '',
        'app_secret' => '',
        'app_code' => '',

        // API url
        'request_url' => 'https://kuaidid.market.alicloudapi.com/danhao',
    ],

    // 支付方式
    'payments' => [
        'wxpay' => '微信',
        'alipay' => '支付宝'
    ],

    // 存储方式
    'storages' => [
        'local' => '本地(服务器)',
        'qiniu' => '七牛云Kodo',
        'tencentyun' => '腾讯云COS',
        'aliyun' => '阿里云OSS'
    ],

    'other' => [

    ],
];
